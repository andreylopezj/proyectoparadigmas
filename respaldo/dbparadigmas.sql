-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2020 a las 06:26:22
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbparadigmas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcanal`
--

CREATE TABLE `tbcanal` (
  `tbcanalid` int(11) NOT NULL,
  `tbcanalnombre` varchar(150) NOT NULL,
  `tbcanalnumero` int(11) NOT NULL,
  `tbcanalicono` varchar(50) NOT NULL,
  `tbcanalsubtitulos` tinyint(1) NOT NULL,
  `tbcanalidioma` varchar(100) NOT NULL,
  `tbcanalcategoria` varchar(100) NOT NULL,
  `tbcanalpais` varchar(100) NOT NULL,
  `tbcanalestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbcanal`
--

INSERT INTO `tbcanal` (`tbcanalid`, `tbcanalnombre`, `tbcanalnumero`, `tbcanalicono`, `tbcanalsubtitulos`, `tbcanalidioma`, `tbcanalcategoria`, `tbcanalpais`, `tbcanalestado`) VALUES
(5, 'tnt', 504, 'lol.jpg', 0, 'Ingles', 'Internacionales', 'Costa Rica', 1),
(6, 'Fox', 33, 'fondo3.jpg', 1, 'Español', 'Internacionales', 'España', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

CREATE TABLE `tbcliente` (
  `tbclienteid` int(11) NOT NULL,
  `tbclienteidentificacion` int(11) NOT NULL,
  `tbclientenombre` varchar(100) NOT NULL,
  `tbclienteapellido` varchar(100) NOT NULL,
  `tbclientecodcliente` int(11) NOT NULL,
  `tbclientecontrasena` varchar(100) NOT NULL,
  `tbclienteusuario` varchar(100) NOT NULL,
  `tbclienterol` varchar(100) NOT NULL,
  `tbclienteestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`tbclienteid`, `tbclienteidentificacion`, `tbclientenombre`, `tbclienteapellido`, `tbclientecodcliente`, `tbclientecontrasena`, `tbclienteusuario`, `tbclienterol`, `tbclienteestado`) VALUES
(1, 1, 'Admin', 'Admin', 0, 'admin', 'admin', 'Administrador', 1),
(2, 207470711, 'Jose', 'Cascante', 3, '123', 'josed', 'Cliente', 1),
(3, 2147483647, 'Anthony', 'Abarca', 3, '123', 'chilas94', 'Tecnico', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbclientecontacto`
--

CREATE TABLE `tbclientecontacto` (
  `tbclientecontactoid` int(11) NOT NULL,
  `tbclienteid` int(11) NOT NULL,
  `tbclientecontactotelefono` int(8) NOT NULL,
  `tbclientecontactocorreo` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tbclientecontactoestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tbclientecontacto`
--

INSERT INTO `tbclientecontacto` (`tbclientecontactoid`, `tbclienteid`, `tbclientecontactotelefono`, `tbclientecontactocorreo`, `tbclientecontactoestado`) VALUES
(1, 6, 55555, 'andrey@hotmail.co', 0),
(2, 2, 62849405, 'josedavid.24@hotmail.com', 1),
(3, 2, 62849405, 'josedavid.24@hotmail.com', 1),
(4, 3, 64785844, 'chilas94@hotmail.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbdireccion`
--

CREATE TABLE `tbdireccion` (
  `tbdireccionid` int(11) NOT NULL,
  `tbdireccionidcliente` int(10) NOT NULL,
  `tbdireccionprovincia` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tbdireccioncanton` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tbdirecciondistrito` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tbdireccionexacta` varchar(400) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tbdireccion`
--

INSERT INTO `tbdireccion` (`tbdireccionid`, `tbdireccionidcliente`, `tbdireccionprovincia`, `tbdireccioncanton`, `tbdirecciondistrito`, `tbdireccionexacta`) VALUES
(1, 7, 'San José', '\r\n         Central', '\r\n         Carmen', '\r\n         cvb'),
(2, 2, '         Alajuela', '         Río Cuarto', '         Río Cuarto', '         450 mts sur oficina BCR Rio Cuarto centro'),
(3, 2, '\r\n         Alajuela', '\r\n         Río Cuarto', '\r\n         Río Cuarto', '\r\n         500 mts sur oficina BCR Rio Cuarto centro'),
(4, 3, '\r\n         Alajuela', '\r\n         Río Cuarto', '\r\n         Río Cuarto', '\r\n         500 mts sur oficina bcr rio cuarto'),
(5, 3, '\r\n         Alajuela', '\r\n         Río Cuarto', '\r\n         Río Cuarto', '\r\n         500 mts sur oficina bcr rio cuarto'),
(6, 4, '\r\n         Cartago', '\r\n         Turrialba', '\r\n         La Suiza', '\r\n         La suiza '),
(7, 4, '\r\n         Cartago', '\r\n         Turrialba', '\r\n         La Suiza', '\r\n         La suiza ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbsolicitud`
--

CREATE TABLE `tbsolicitud` (
  `tbsolicitudid` int(11) NOT NULL,
  `tbsolicitudnumeroabonado` int(11) NOT NULL,
  `tbsolicitudnumeroorden` int(11) NOT NULL,
  `tbsolicitudfechasolicitud` date NOT NULL,
  `tbsolicitudestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbsolicitud`
--

INSERT INTO `tbsolicitud` (`tbsolicitudid`, `tbsolicitudnumeroabonado`, `tbsolicitudnumeroorden`, `tbsolicitudfechasolicitud`, `tbsolicitudestado`) VALUES
(1, 1, 1, '0000-00-00', 1),
(2, 32, 156, '2020-05-14', 1),
(3, 99, 45, '2020-08-18', 1),
(4, 45, 1, '2020-09-02', 1),
(5, 50, 15, '2020-10-09', 1),
(6, 87, 5454, '2020-11-24', 1),
(7, 77, 1253, '2020-07-31', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtecnico`
--

CREATE TABLE `tbtecnico` (
  `tbtecnicoid` int(11) NOT NULL,
  `tbtecnicocedula` int(11) NOT NULL,
  `tbtecniconombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicoapellidos` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicousuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicocontrasenia` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicotelefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicocorreo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicoestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbtecnico`
--

INSERT INTO `tbtecnico` (`tbtecnicoid`, `tbtecnicocedula`, `tbtecniconombre`, `tbtecnicoapellidos`, `tbtecnicousuario`, `tbtecnicocontrasenia`, `tbtecnicotelefono`, `tbtecnicocorreo`, `tbtecnicoestado`) VALUES
(1, 702600489, 'Andreyy', 'Lopez Picadoo', 'and.144', '12345', '88658910', 'andrey@hotmail.com', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbcanal`
--
ALTER TABLE `tbcanal`
  ADD PRIMARY KEY (`tbcanalid`);

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`tbclienteid`);

--
-- Indices de la tabla `tbclientecontacto`
--
ALTER TABLE `tbclientecontacto`
  ADD PRIMARY KEY (`tbclientecontactoid`);

--
-- Indices de la tabla `tbdireccion`
--
ALTER TABLE `tbdireccion`
  ADD PRIMARY KEY (`tbdireccionid`);

--
-- Indices de la tabla `tbsolicitud`
--
ALTER TABLE `tbsolicitud`
  ADD PRIMARY KEY (`tbsolicitudid`);

--
-- Indices de la tabla `tbtecnico`
--
ALTER TABLE `tbtecnico`
  ADD PRIMARY KEY (`tbtecnicoid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
