-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2020 a las 06:27:01
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbparadigmas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbtecnico`
--

CREATE TABLE `tbtecnico` (
  `tbtecnicoid` int(11) NOT NULL,
  `tbtecnicocedula` int(11) NOT NULL,
  `tbtecniconombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicoapellidos` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicousuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicocontrasenia` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicotelefono` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicocorreo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tbtecnicoestado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tbtecnico`
--

INSERT INTO `tbtecnico` (`tbtecnicoid`, `tbtecnicocedula`, `tbtecniconombre`, `tbtecnicoapellidos`, `tbtecnicousuario`, `tbtecnicocontrasenia`, `tbtecnicotelefono`, `tbtecnicocorreo`, `tbtecnicoestado`) VALUES
(1, 702600489, 'Andreyy', 'Lopez Picadoo', 'and.144', '12345', '88658910', 'andrey@hotmail.com', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbtecnico`
--
ALTER TABLE `tbtecnico`
  ADD PRIMARY KEY (`tbtecnicoid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
