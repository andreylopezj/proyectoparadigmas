<?php

include '../datos/consultasCanal.php';

class canalNegocio {

    private $consultasCanal;

    public function canalNegocio() {
        $this->consultasCanal = new consultasCanal();
    }

    public function insertCanal($canal) {
        return $this->consultasCanal->insertCanal($canal);
    }

    public function modificar($canal) {
        return $this->consultasCanal->modificar($canal);
    }

    public function eliminarCable($canal) {
        return $this->consultasCanal->eliminarCable($canal);
    }

    public function getCanal() {
        return $this->consultasCanal->getCanales();
    }
    
    
}

