<?php

include '../datos/consultasPeticionTecnico.php';

class peticionTecnicoNegocio{
    private $consultasPeticionTecnico;

    public function PeticionTecnicoNegocio() {
        $this->consultasPeticionTecnico = new ConsultasPeticionTecnico();
    }

    public function insertar($peticionTecnico) {
        return $this->consultasPeticionTecnico->insertar($peticionTecnico);
    }

    public function modificar($peticionTecnico) {
        return $this->consultasPeticionTecnico->modificar($peticionTecnico);
    }

    public function eliminar($peticionTecnico) {
        return $this->consultasPeticionTecnico->eliminar($peticionTecnico);
    }
    
    public function getPeticionTecnico() {
        return $this->consultasPeticionTecnico->getPeticionTecnico();
    }


}

