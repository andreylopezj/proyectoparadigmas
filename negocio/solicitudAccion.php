<?php

include './solicitudNegocio.php';

if (isset($_POST['registrar'])) {

    $id = 0;
    $numeroAbonado = $_POST['numeroAbonado'];
    $numeroOrden = $_POST['numeroOrden'];
    $fechaSolicitud = $_POST['fechaSolicitud'];
    $estado =1; //$_POST['estado'];

    $temp=0;
    $temp1=0;
    $solicitudNegocio = new SolicitudNegocio();
    $solicitudes = $solicitudNegocio->getSolicitudes();
    
    foreach($solicitudes as $solicitud){
        if ($solicitud->getNumeroAbonado()==$numeroAbonado) {
            $temp=1;
        }else {
            if ($solicitud->getNumeroOrden()==$numeroOrden) {
                $temp1=1;
            }
        }
    }

    if ($temp==0&&$temp1==0) {
        if(!empty($numeroAbonado)&&!empty($numeroOrden)&&!empty($fechaSolicitud)){
            $solicitud = new Solicitud($id,$numeroAbonado,$numeroOrden,$fechaSolicitud,$estado);
            $solicitudNegocio = new solicitudNegocio();
            $result = $solicitudNegocio->insertarSolicitud($solicitud);
        
            if ($result == 1) {
                header("location: ../vista/solicitudVista.php?exito=inserto");
            }else {
                header("location: ../vista/solicitudVista.php?error=dbError");
            } 
        }else{
            header("location: ../vista/solicitudVista.php?error=campos");
        }
    }else {
        header("location: ../vista/solicitudVista.php?error=iguales");
    }
} else if (isset($_POST['eliminar'])) {

    if (isset($_POST['id'])) {

        $id = $_POST['id'];
        $numeroAbonado = $_POST['numeroAbonado'];
        $numeroOrden = $_POST['numeroOrden'];
        $fechaSolicitud = $_POST['fechaSolicitud'];
        $estado = 0;
           
            $solicitud = new Solicitud($id,$numeroAbonado,$numeroOrden, $fechaSolicitud, $estado);

            $solicitudNegocio = new solicitudNegocio();

            $result = $solicitudNegocio->editarSolicitud($solicitud);
            if ($result == 1) {
                header("location: ../vista/solicitudVista.php?exito=elimino");
            } else {
                header("location: ../vista/solicitudVista.php?error=dbError");
            }
    } else {
        header("location: ../vista/solicitudVista.php?error=error");
    }
}
else if (isset($_POST['editar'])) {

    $id = $_POST['id'];
    $numeroAbonado = $_POST['numeroAbonado'];
    $numeroOrden = $_POST['numeroOrden'];
    $fechaSolicitud = $_POST['fechaSolicitud'];
    $estado = 1;

    $temp=0;
    $solicitudNegocio = new SolicitudNegocio();
    $solicitudes = $solicitudNegocio->getSolicitudes();
    
    foreach($solicitudes as $solicitud){
        if ($solicitud->getNumeroOrden()==$numeroOrden) {
            $temp=1;
        }
    }
    if ($temp==0) {
        if(!empty($numeroAbonado)&&!empty($numeroOrden)&&!empty($fechaSolicitud)){   
            $solicitud = new Solicitud($id,$numeroAbonado,$numeroOrden, $fechaSolicitud, $estado);

            $solicitudNegocio = new solicitudNegocio();

            $result = $solicitudNegocio->editarSolicitud($solicitud);
            if ($result == 1) {
                header("location: ../vista/solicitudVista.php?exito=edito");
            } else {
                header("location: ../vista/solicitudVista.php?error=dbError");
            }
        }else{
            header("location: ../vista/solicitudVista.php?error=campos");
        }
    }else{
        header("location: ../vista/solicitudVista.php?error=iguales");
    }

}
