<?php

include './clienteNegocio.php';
include './contactoNegocio.php';

if (isset($_POST['editar'])) {

        $id = $_POST['id'];
        $identificacion = $_POST['identificacion'];
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $codcliente = $_POST['codcliente'];
        $contrasena = $_POST['contrasena'];
        $usuario = $_POST['usuario'];
        $rol = $_POST['rol'];
        $estado = 1;
           
            $cliente = new Cliente($id,$identificacion,$nombre, $apellido, $codcliente, $contrasena, $usuario, $rol, $estado);

            $clienteNegocio = new ClienteNegocio();

            $result = $clienteNegocio->editarCliente($cliente);
            if ($result == 1) {
                header("location: ../vista/clienteVista.php?success=updated");
            } else {
                header("location: ../vista/clienteVista.php?error=dbError");
            }
    
} 
else if (isset($_POST['eliminar'])) {

    if (isset($_POST['id'])) {

        $id = $_POST['id'];
 
        $clienteNegocio = new ClienteNegocio();
        $result = $clienteNegocio->eliminarCliente($id);

        if ($result == 1) {
            header("location: ../vista/clienteVista.php?success=deleted");
        } else {
            header("location: ../vista/clienteVista.php?error=dbError");
        }
    } else {
        header("location: ../vista/clienteVista.php?error=error");
    }
}
else if (isset($_POST['registrar'])) {

    //Recuperacion de datos 
    $id = 0;
    $identificacion = $_POST['identificacion'];
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $codcliente = 3;//$_POST['codcliente'];
    $contrasena = $_POST['contrasena'];
    $usuario = $_POST['usuario'];
    $rol=$_POST['rol'];
    $estado = 1;

    //------------------------ Recuperacion de datos de contactos-----------------------------
    $itemsTelefono = ($_POST['telefono']);
	$itemsCorreo = ($_POST['correo']);


    //Guardado en DB
    $cliente = new Cliente($id,$identificacion,$nombre,$apellido,$codcliente,$contrasena,$usuario,$rol,$estado);
    $clienteNegocio = new ClienteNegocio();
    $result = $clienteNegocio->insertarCliente($cliente);

    //------ codigo nuevo adrian----------------------

    $ultimoIdCliente = $clienteNegocio->obtenerUltimoId();
    $contactoNegocio = new ContactoNegocio();
    $result2 = $contactoNegocio->insertarContacto($ultimoIdCliente, $itemsTelefono, $itemsCorreo);


    if ($result == 1) {
        if($result2 == 1)
            header("location: ../vista/clienteVista.php?success=insert");
        else
            echo 'no funciono el registro de contacto';
    }
    else {
        echo 'no funcino el registro del cliente';
        //header("location: ../vista/inicio.php?error=dbError");
    }

}elseif(isset($_POST['sesion'])){

    $usuario=$_POST['usuario'];
    $contrasena=$_POST['contrasena'];

    $clienteNegocio = new ClienteNegocio();

    $result = $clienteNegocio->inicioSesion($usuario,$contrasena);

    if ($result=="admin") {
        header("location: ../vista/inicio.php");
    }else if($result=="cliente"){
    header("location: ../vista/inicioCliente.php");
    }else if($result=="Tecnico"){
        header("location: ../vista/tecnicoVista.php");
        }else {
        header("location: ../vista/login.php?error=dbError");
    }

}
