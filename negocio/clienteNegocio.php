<?php

include '../datos/consultasCliente.php';

class ClienteNegocio{

    private $consultasCliente;

    public function ClienteNegocio() {
        $this->consultasCliente = new ConsultasCliente();
    }

    public function insertarCliente($cliente) {
        return $this->consultasCliente->insertarCliente($cliente);
    }

    public function editarCliente($id) {
        return $this->consultasCliente->editarCliente($id);
    }

    public function eliminarCliente($id) {
        return $this->consultasCliente->eliminarCliente($id);
    }

    public function getClientes() {
        return $this->consultasCliente->getClientes();
    }

    //---------- codigo para consultar el ultimo ud del cliente
    public function obtenerUltimoId(){
        return $this->consultasCliente->obtenerUltimoId();
    }
    
    public function inicioSesion($usuario,$contrasena){
        return $this->consultasCliente->inicioSesion($usuario,$contrasena);
    }
    
}

