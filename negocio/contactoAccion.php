<?php
    include './contactoNegocio.php';

    if(isset($_POST['editar'])){

        $idContacto = $_POST['idcontacto'];
        $telefono = $_POST['telefono'];
        $correo = $_POST['correo'];

        //modificar
        $contacto = new Contacto($idContacto, 0, $telefono, $correo);

        $contactoNegocio = new ContactoNegocio();

        $result = $contactoNegocio->editarContacto($contacto);
        if ($result == 1) {
            header("location: ../vista/clienteVista.php?success=updated");
        } else {
            header("location: ../vista/clienteVista.php?error=dbError");
        }
    }
    elseif (isset($_POST['eliminar'])) {
        
        if (isset($_POST['idcontacto'])) {

            $id = $_POST['idcontacto'];
     
            $contactoNegocio = new ContactoNegocio();
            $result = $contactoNegocio->eliminarContacto($id);
    
            if ($result == 1) {
                header("location: ../vista/clienteVista.php?success=deleted");
            } else {
                header("location: ../vista/clienteVista.php?error=dbError");
            }
        } else {
            header("location: ../vista/clienteVista.php?error=error");
        }
    }

?>