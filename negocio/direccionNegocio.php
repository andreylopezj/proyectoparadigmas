<?php

include '../datos/consultasDireccion.php';

class direccionNegocio {

    private $consultasDireccion;

    public function direccionNegocio() {
        $this->consultasDireccion = new consultasDireccion();
    }

    public function insertDireccion($direcciones) {
        
        foreach ($direcciones as $fila) {
            $provincia = $fila['provincia'];
            $canton = $fila['canton'];
            $distrito = $fila['distrito'];
            $exacta = $fila['exacta'];
            $direccion = new Direccion(0,$provincia,$canton,$distrito,$exacta);
            $this->consultasDireccion->insertDireccion($direccion);
        }
        
    }

    public function modificarDireccion($direccion) {
        return $this->consultasDireccion->modificar($direccion);
    }

    public function eliminarDireccion($idDireccion) {
        return $this->consultasDireccion->eliminarDireccion($idDireccion);
    }

    public function getDireccion($idCliente) {
        return $this->consultasDireccion->getDirecciones($idCliente);
    }
    
    
}