<?php

include '../datos/consultasAsignacionTecnico.php';

class AsignacionTecnicoNegocio{

    private $consultasAsignacionTecnico;

    public function AsignacionTecnicoNegocio() {
        $this->consultasAsignacionTecnico = new ConsultasAsignacionTecnico();
    }

    public function insertarAsignacionTecnico($asignacionTecnico) {
        return $this->consultasAsignacionTecnico->insertarAsignacionTecnico($asignacionTecnico);
    }

    public function editarAsignacionTecnico($asignacionTecnico) {
        return $this->consultasAsignacionTecnico->editarAsignacionTecnico($asignacionTecnico);
    }

    public function eliminarAsignacionTecnico($id) {
        return $this->consultasAsignacionTecnico->eliminarAsignacionTecnico($id);
    }

    public function getAsignacionTecnico() {
        return $this->consultasAsignacionTecnico->getAsignacionTecnico();
    }
    
}

