<?php

include './asignacionTecnicoNegocio.php';

if (isset($_POST['registrar'])) {

    $id = 0;
    $idSolicitud = $_POST['idsolicitud'];
    $nombreTecnico = $_POST['nombre'];
    $tipoTrabajo = $_POST['tipoTrabajo'];
    $estado =1;


    if(!empty($idSolicitud) && !empty($nombreTecnico) && !empty($tipoTrabajo)){

        $asignacion = new AsignacionTecnico($id, $idSolicitud, $nombreTecnico, $tipoTrabajo, $estado);
        $asignacionNegocio = new AsignacionTecnicoNegocio();
        $result = $asignacionNegocio->insertarAsignacionTecnico($asignacion);
        
        if ($result == 1) {
            header("location: ../vista/asignacionTecnicoVista.php?exito=inserto");
        }else {
            header("location: ../vista/asignacionTecnicoVista.php?error=dbError");
        } 
    }else{
        header("location: ../vista/asignacionTecnicoVista.php?error=campos");
    }
    
}
else if (isset($_POST['eliminar'])) {

    if (isset($_POST['id'])) {

        $id = $_POST['id'];

        $asignacionNegocio = new AsignacionTecnicoNegocio();

        $result = $asignacionNegocio->eliminarAsignacionTecnico($id);
        if ($result == 1) {
            header("location: ../vista/asignacionTecnicoVista.php?exito=elimino");
        } else {
            header("location: ../vista/asignacionTecnicoVista.php?error=dbError");
        }
    } else {
        header("location: ../vista/asignacionTecnicoVista.php?error=error");
    }
}
else if (isset($_POST['editar'])) {

    $id = $_POST['id'];
    $tipoTrabajo = $_POST['trabajoTipo'];
    $estado =1;

    if(!empty($tipoTrabajo)){

        $asignacion = new AsignacionTecnico($id, 0, "", $tipoTrabajo, $estado);
        $asignacionNegocio = new AsignacionTecnicoNegocio();
        $result = $asignacionNegocio->editarAsignacionTecnico($asignacion);

        if ($result == 1) {
            header("location: ../vista/asignacionTecnicoVista.php?exito=edito");
        } else {
            header("location: ../vista/asignacionTecnicoVista.php?error=dbError");
        }
    }else{
        header("location: ../vista/asignacionTecnicoVista.php?error=campos");
    }

}
