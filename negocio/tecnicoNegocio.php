<?php

include '../datos/consultasTecnico.php';

class TecnicoNegocio{

    private $consultasTecnico;

    public function TecnicoNegocio() {
        $this->consultasTecnico = new ConsultasTecnico();
    }

    public function insertarTecnico($tecnico) {
        return $this->consultasTecnico->insertar($tecnico);
    }

    public function editarTecnico($tecnico) {
        return $this->consultasTecnico->editar($tecnico);
    }

    public function eliminarTecnico($id) {
        return $this->consultasTecnico->eliminar($id);
    }

    public function getTecnicos() {
        return $this->consultasTecnico->getTecnicos();
    }

    
}