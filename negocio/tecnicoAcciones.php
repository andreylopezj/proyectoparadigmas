<?php

include './tecnicoNegocio.php';

if (isset($_POST['editar'])) {
    $id = $_POST['id'];
    $cedula = $_POST['cedula'];
    $nombre = $_POST['nombre'];
    $apellidos = $_POST['apellidos'];
    $usuario = $_POST['usuario'];
    $contrasenia = $_POST['contrasenia'];
    $telefono = $_POST['telefono'];
    $correo = $_POST['correo'];
    $tecnico = new Tecnico($id,$cedula,$nombre,$apellidos,$usuario,$contrasenia,$telefono,$correo);

    $tecnicoNegocio = new TecnicoNegocio();
    $result = $tecnicoNegocio->editarTecnico($tecnico);
    if ($result == 1) {
        header("location: ../vista/tecnicoVista.php?success=updated");
    } else {
        header("location: ../vista/tecnicoVista.php?error=dbError");
    }
    
} else if (isset($_POST['eliminar'])) {
    $id = $_POST['id'];
    $tecnicoNegocio = new TecnicoNegocio();
    $result = $tecnicoNegocio->eliminarTecnico($id);
    if ($result == 1) {
        header("location: ../vista/tecnicoVista.php?success=updated");
    } else {
        header("location: ../vista/tecnicoVista.php?error=dbError");
    }
   
}else if (isset($_POST['registrar'])) {
    $cedula = $_POST['cedula'];
    $nombre = $_POST['nombre'];
    $apellidos = $_POST['apellidos'];
    $usuario = $_POST['usuario'];
    $contrasenia = $_POST['contrasenia'];
    $telefono = $_POST['telefono'];
    $correo = $_POST['correo'];

    $tecnico = new Tecnico(0,$cedula,$nombre,$apellidos,$usuario,$contrasenia,$telefono,$correo);
    $tecnicoNegocio = new TecnicoNegocio();
    $result = $tecnicoNegocio->insertarTecnico($tecnico);
    if ($result == 1) {
        header("location: ../vista/tecnicoVista.php?success=updated");
    } else {
        header("location: ../vista/tecnicoVista.php?error=dbError");
    }
    
}