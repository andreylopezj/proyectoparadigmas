<?php

include './peticionTecnicoNegocio.php';


if(isset($_POST['crear'])){
    $id = 0;
    $fecha = $_POST['fechaCreacion'];
    $cambioEquipo = 1;
    $descripcion =$_POST['descripcion'];
    $idTecnico = $_POST['idTecnico'];
    $estado = 1;


    $peticionTecnico = new peticionTecnico($id,$fecha,$cambioEquipo,$descripcion,$idTecnico,$estado);

    $peticionTecnicoNegocio = new peticionTecnicoNegocio();

    $result = $peticionTecnicoNegocio->insertar($peticionTecnico);
    if ($result == 1) {
        header("location: ../vista/atencionPeticionTecnicoVista.php?success=deleted");
    } else {
        header("location: ../vista/atencionPeticionTecnicoVista.php?error=dbError");
    }
   

}else if(isset($_POST['eliminar'])){

    if (isset($_POST['id'])) {

        $id = $_POST['id'];
 
        $peticionTecnicoNegocio = new peticionTecnicoNegocio();
        $result = $peticionTecnicoNegocio->eliminar($id);

        if ($result == 1) {
            header("location: ../vista/atencionPeticionTecnicoVista.php?success=deleted");
        } else {
            header("location: ../vista/atencionPeticionTecnicoVista.php?error=dbError");
        }
    }
}else if(isset($_POST['modificar'])){
    $id =$_POST['id'];
    $fecha = $_POST['fechaCreacion'];
    $cambioEquipo =1;
    $descripcion = $_POST['descripcion'];
    $idTecnico = $_POST['idTecnico'];
    $estado = 1;
       
            $peticionTecnico = new peticionTecnico($id,$fecha, $cambioEquipo, $descripcion, $idTecnico, $estado);

            $peticionTecnicoNegocio = new peticionTecnicoNegocio();

            $result = $peticionTecnicoNegocio->modificar($peticionTecnico);
            if ($result == 1) {
                header("location: ../vista/atencionPeticionTecnicoVista.php?success=updated");
            } else {
                header("location: ../vista/atencionPeticionTecnicoVista.php?error=dbError");
            }

}

