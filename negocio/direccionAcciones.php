<?php

include './direccionNegocio.php';

if (isset($_POST['editar'])) {
    
    $nombre = $_GET['nom'];
    $apellidos = $_GET['ape'];

    $idDireccion = $_POST['idDireccion'];
    $idCliente = $_POST['idCliente'];
    $provincia = $_POST['provincia'];
    $canton = $_POST['canton'];
    $distrito = $_POST['distrito'];
    $exacta = $_POST['exacta'];

    $direccion = new Direccion($idDireccion,$provincia,$canton,$distrito,$exacta);
    $direccionNegocio = new direccionNegocio();
    $result = $direccionNegocio->modificarDireccion($direccion);
    echo($direccion->getCanton());
    if ($result == 1) {
        header("location: ../vista/contactoclientevista.php?id=$idCliente&nom=$nombre&ape=$apellidos");
    } else {
        header("location: ../vista/contactoclientevista.php?error=dbError");
    }
    
} else if (isset($_POST['eliminar'])) {

    $idCliente = $_POST['idCliente'];
    $idDireccion = $_POST['idDireccion'];
    $nombre = $_GET['nom'];
    $apellidos = $_GET['ape'];
    $direccionNegocio = new direccionNegocio();
    $result = $direccionNegocio->eliminarDireccion($idDireccion);
    if ($result == 1) {
        header("location: ../vista/contactoclientevista.php?id=$idCliente&nom=$nombre&ape=$apellidos");
    } else {
        header("location: ../vista/contactoclientevista.php?error=dbError");
    }
   
}else if (isset($_POST['valores'])) {

    $direcciones = json_decode($_POST['valores'], true);
    $direccionNegocio = new direccionNegocio();
    $result = $direccionNegocio->insertDireccion($direcciones);
}