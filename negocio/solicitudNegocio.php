<?php

include '../datos/consultasSolicitud.php';

class SolicitudNegocio{

    private $consultasSolicitud;

    public function SolicitudNegocio() {
        $this->consultasSolicitud = new ConsultasSolicitud();
    }

    public function insertarSolicitud($solicitud) {
        return $this->consultasSolicitud->insertarSolicitud($solicitud);
    }

    public function editarSolicitud($id) {
        return $this->consultasSolicitud->editarSolicitud($id);
    }

    public function eliminarSolicitud($id) {
        return $this->consultasSolicitud->eliminarSolicitud($id);
    }

    public function getSolicitudes() {
        return $this->consultasSolicitud->getSolicitudes();
    }

    
}

