<?php
    include '../datos/consultasContactos.php';

    class ContactoNegocio{

        private $consultasContactos;

        public function ContactoNegocio() {
            $this->consultasContactos = new ConsultasContactos();
        }

        public function insertarContacto($ultimoIdCliente, $itemsTelefono, $itemsCorreo) {
            return $this->consultasContactos->insertarContacto($ultimoIdCliente, $itemsTelefono, $itemsCorreo);
        }

        public function getContactoCliente($idCliente) {
            return $this->consultasContactos->getContactoCliente($idCliente);
        }

        public function editarContacto($contacto) {
            return $this->consultasContactos->editarContacto($contacto);
        }
    
        public function eliminarContacto($id) {
            return $this->consultasContactos->eliminarContacto($id);
        }

    }

?>