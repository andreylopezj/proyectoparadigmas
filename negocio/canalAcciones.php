<?php

include './canalNegocio.php';

if (isset($_POST['modificar'])) {
        $idCanal = $_POST['idcanal'];
        $nombre = $_POST['nombre'];
        $numero = $_POST['numero'];
        $icono = $_POST['icono'];
        $subtitulos = $_POST['subtitulos'];
        $idioma = $_POST['idioma'];
        $categoria = $_POST['categoria'];
        $pais = $_POST['pais'];
        $estado = 1;
           
                $canal = new Canal($idCanal,$nombre, $numero, $icono, $subtitulos, $idioma, $categoria, $pais,$estado);

                $canalNegocio = new CanalNegocio();

                $result = $canalNegocio->modificar($canal);
                if ($result == 1) {
                    header("location: ../vista/canalVista.php?success=updated");
                } else {
                    header("location: ../vista/canalVista.php?error=dbError");
                }
                $canal = new Cable($idCanal,$nombre, $numero, $icono, $subtitulos, $idioma, $categoria, $pais,$estado);

                $canalNegocio = new canalNegocio();

                $result = $canalNegocio->modificar($canal);
                header("location: ../vista/canalVista.php");
               
            
       
    
} else if (isset($_POST['eliminar'])) {

    if (isset($_POST['idcanal'])) {

        $idCable = $_POST['idcanal'];
 
        $canalNegocio = new CanalNegocio();
        $result = $canalNegocio->eliminarCable($idCable);

        if ($result == 1) {
            header("location: ../vista/canalVista.php?success=deleted");
        } else {
            header("location: ../vista/canalVista.php?error=dbError");
        }
    } else {
        header("location: ../vista/canalVista.php?error=error");
    }
}else if (isset($_POST['create'])) {

    //Recuperacion de datos 
    $id = 0;
    $nombre = $_POST['nombre'];
    $numero = $_POST['numero'];
    //$icono = $_FILES['icono']['name'];
    $icono = $_POST['icono'];
    $subtitulos = $_POST['subtitulos'];
    if($subtitulos == 'Si'){
        $subtitulos=1;
    }else{
        $subtitulos=0;
    }
    $idioma = $_POST['idioma'];
    $categoria = $_POST['categoria'];
    $pais = $_POST['pais'];
    $estado = 1;
    //Guardado en DB
    $canal = new Canal($id,$nombre,$numero,$icono,$subtitulos,$idioma,$categoria,$pais,$estado);
    $canalNegocio = new canalNegocio();
    $result = $canalNegocio->insertCanal($canal);
    header('Location: ../vista/canalVista.php');
}