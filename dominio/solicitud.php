<?php 
//solicitude de mantenieminto de cliente
class Solicitud{

    private $id;
    private $numeroOrden;
    private $numeroAbonado;
    private $fechaSolicitud;
    private $estado;

    function Solicitud($id,$numeroAbonado,$numeroOrden,$fechaSolicitud,$estado){
        $this->id=$id;
        $this->numeroAbonado=$numeroAbonado;
        $this->numeroOrden=$numeroOrden;
        $this->fechaSolicitud=$fechaSolicitud;
        $this->estado=$estado;
    }



    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of numeroOrden
     */ 
    public function getNumeroOrden()
    {
        return $this->numeroOrden;
    }

    /**
     * Set the value of numeroOrden
     *
     * @return  self
     */ 
    public function setNumeroOrden($numeroOrden)
    {
        $this->numeroOrden = $numeroOrden;

        return $this;
    }

    /**
     * Get the value of numeroAbonado
     */ 
    public function getNumeroAbonado()
    {
        return $this->numeroAbonado;
    }

    /**
     * Set the value of numeroAbonado
     *
     * @return  self
     */ 
    public function setNumeroAbonado($numeroAbonado)
    {
        $this->numeroAbonado = $numeroAbonado;

        return $this;
    }

    /**
     * Get the value of fechaSolicitud
     */ 
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set the value of fechaSolicitud
     *
     * @return  self
     */ 
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
}

?>