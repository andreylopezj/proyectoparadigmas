<?php

class PeticionTecnico{


    private $id;
    private $fecha;
    private $cambioEquipo;
    private $descripcion;
    private $idTecnico;
    private $estado;


    function peticionTecnico($id,$fecha,$cambioEquipo,$descripcion,$idTecnico,$estado){
        $this->id = $id;
        $this->fecha = $fecha;
        $this->cambioEquipo = $cambioEquipo;
        $this->descripcion = $descripcion;
        $this->idTecnico = $idTecnico;
        $this->estado = $estado;
    }

    public function getId()
    {
        return $this->id;
    }

    
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getCambioEquipo()
    {
        return $this->cambioEquipo;
    }

    
    public function setCambioEquipo($cambioEquipo)
    {
        $this->cambioEquipo = $cambioEquipo;

        return $this;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getIdTecnico()
    {
        return $this->idTecnico;
    }

    
    public function setIdTecnico($idTecnico)
    {
        $this->idTecnico = $idTecnico;

        return $this;
    }
    public function getEstado()
    {
        return $this->estado;
    }

    
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }



    
}




