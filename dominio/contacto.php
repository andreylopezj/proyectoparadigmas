<?php
class Contacto{
    private $id;
    private $clienteid;
    private $telefono;
    private $correo;


    function Contacto($id, $clienteid, $telefono, $correo){
        $this->id = $id;
        $this->clienteid = $clienteid;
        $this->telefono = $telefono;
        $this->correo = $correo;
    }


    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getClienteId() {
        return $this->clienteid;
    }
    public function setClienteId($clienteid) {
        $this->clienteid = $clienteid;
    }

    public function getTelefono() {
        return $this->telefono;
    }
    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    public function getCorreo() {
        return $this->correo;
    }
    public function setCorreo($correo) {
        $this->correo = $correo;
    }

}
?>

