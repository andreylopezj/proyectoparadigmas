<?php

class Tecnico{
    private $id;
    private $cedula;
    private $nombre;
    private $apellidos;
    private $usuario;
    private $contrasenia;
    private $telefono;
    private $correo;

    function Tecnico($id,$cedula,$nombre,$apellidos,$usuario,$contrasenia,$telefono,$correo) {
        $this->id = $id;
        $this->cedula = $cedula;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->usuario = $usuario;
        $this->contrasenia = $contrasenia;
        $this->telefono = $telefono;
        $this->correo = $correo;
    }

    //Id
    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    //Cedula
    public function getCedula() {
        return $this->cedula;
    }
    public function setCedula($cedula) {
        $this->cedula = $cedula;
    }

    //Nombre
    public function getNombre() {
        return $this->nombre;
    }
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    //Apellidos
    public function getApellidos() {
        return $this->apellidos;
    }
    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    //Usuario
    public function getUsuario() {
        return $this->usuario;
    }
    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    //Contraseña
    public function getContrasenia() {
        return $this->contrasenia;
    }
    public function setContrasenia($contrasenia) {
        $this->contrasenia = $contrasenia;
    }

    //Telefono
    public function getTelefono() {
        return $this->telefono;
    }
    public function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    //Correo
    public function getCorreo() {
        return $this->correo;
    }
    public function setCorreo($correo) {
        $this->correo = $correo;
    }

}
