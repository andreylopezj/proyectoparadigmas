<?php
class AsignacionTecnico{
    private $id;
    private $idsolicitud;
    private $nombreTecnico;
    private $tipoTrabajo;


    function AsignacionTecnico($id, $idsolicitud, $nombreTecnico, $tipoTrabajo){
        $this->id = $id;
        $this->idsolicitud = $idsolicitud;
        $this->nombreTecnico = $nombreTecnico;
        $this->tipoTrabajo = $tipoTrabajo;
    }


    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getIdSolicitud() {
        return $this->idsolicitud;
    }
    public function setIdSolicitud($idsolicitud) {
        $this->idsolicitud = $idsolicitud;
    }

    public function getNombreTecnico() {
        return $this->nombreTecnico;
    }
    public function setNombreTecnico($nombreTecnico) {
        $this->nombreTecnico = $nombreTecnico;
    }

    public function getTipoTrabajo() {
        return $this->tipoTrabajo;
    }
    public function setTipoTrabajo($tipoTrabajo) {
        $this->tipoTrabajo = $tipoTrabajo;
    }

}
?>

