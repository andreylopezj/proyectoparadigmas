<?php

class Direccion{
    private $id;
    private $provincia;
    private $canton;
    private $distrito;
    private $direcionExacta;

    function Direccion($id,$provincia, $canton,$distrito, $direcionExacta) {
        $this->id = $id;
        $this->provincia = $provincia;
        $this->canton = $canton;
        $this->distrito = $distrito;
        $this->direcionExacta = $direcionExacta;
    }

    //Id
    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    //Provincia
    public function getProvincia() {
        return $this->provincia;
    }
    public function setProvincia($provincia) {
        $this->provincia = $provincia;
    }

    //Canton
    public function getCanton() {
        return $this->canton;
    }
    public function setCanton($canton) {
        $this->canton = $canton;
    }

    //Distrito
    public function getDistrito() {
        return $this->distrito;
    }
    public function setDistrito($distrito) {
        $this->distrito = $distrito;
    }

    //Exacta
    public function getExacta() {
        return $this->direcionExacta;
    }
    public function setExacta($direcionExacta) {
        $this->direcionExacta = $direcionExacta;
    }

}
