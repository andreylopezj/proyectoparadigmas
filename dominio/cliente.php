<?php

class Cliente{

    private $id;
    private $identificacion;
    private $nombre;
    private $apellido;
    private $codcliente;
    private $usuario;
    private $contrasena;
    private $rol;
    private $estado;

    function Cliente($id,$identificacion,$nombre,$apellido,$codcliente,$contrasena,$usuario,$rol,$estado){
        $this->id=$id;
        $this->identificacion=$identificacion;
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->codcliente=$codcliente;
        $this->contrasena=$contrasena;
        $this->usuario=$usuario;
        $this->rol=$rol;
        $this->estado=$estado;
    }
    
     
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getRol() {
        return $this->id;
    }

    public function setRol($rol) {
        $this->rol = $rol;
    }

    public function getIdentificacion() {
        return $this->identificacion;
    }

    public function setIdentificacion($identificacion) {
        $this->identificacion = $identificacion;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
    
    public function getApellido() {
        return $this->apellido;
    }

    public function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    public function getCodCliente() {
        return $this->codcliente;
    }

    public function setCodCliente($codcliente) {
        $this->codcliente = $codcliente;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    public function getContrasena() {
        return $this->contrasena;
    }

    public function setContrasena($contrasena) {
        $this->contrasena = $contrasena;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }
}