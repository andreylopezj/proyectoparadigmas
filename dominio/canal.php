<?php

class Canal{
    private $idCanal;
    private $nombre;
    private $numero;
    private $icono;
    private $subtitulos;
    private $idioma;
    private $categoria;
    private $pais;
    private $estado;

    function Canal($idCanal, $nombre,$numero, $icono, $subtitulos, $idioma, $categoria, $pais,$estado) {
        $this->idCanal = $idCanal;
        $this->nombre = $nombre;
        $this->numero = $numero;
        $this->icono = $icono;
        $this->subtitulos = $subtitulos;
        $this->idioma = $idioma;
        $this->categoria = $categoria;
        $this->pais = $pais;
        $this->estado = $estado; 
    }

    public function getIdCanal() {
        return $this->idCanal;
    }

    public function setIdCanal($idCanal) {
        $this->idCanal = $idCanal;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }
    
    public function getIcono() {
        return $this->icono;
    }

    public function setIcono($icono) {
        $this->icono = $icono;
    }

    public function getSubtitulos() {
        return $this->subtitulos;
    }

    public function setSubtitulos($subtitulos) {
        $this->subtitulos = $subtitulos;
    }

    public function getIdioma() {
        return $this->idioma;
    }

    public function setIdioma($idioma) {
        $this->idioma = $idioma;
    }

    public function getCategoria() {
        return $this->categoria;
    }

    public function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    public function getPais() {
        return $this->pais;
    }

    public function setPais($pais) {
        $this->pais = $pais;
    }

    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }
}
