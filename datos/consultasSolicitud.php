<?php

include_once 'conexion.php';
include '../dominio/solicitud.php';


class ConsultasSolicitud extends Conexion {

    public function insertarSolicitud($solicitud) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

         //Obtiene el ultimo id
         $queryGetLastId = "SELECT MAX(tbsolicitudid) AS tbsolicitudid  FROM tbsolicitud";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         $queryInsert = "INSERT INTO tbsolicitud(tbsolicitudid,tbsolicitudnumeroabonado,tbsolicitudnumeroorden,tbsolicitudfechasolicitud,
         tbsolicitudestado) VALUES ('" . $nextId . "','" .
                $solicitud->getNumeroAbonado() . "','" .
                $solicitud->getNumeroOrden() . "','" .
                 $solicitud->getFechaSolicitud() . "','" .
                 $solicitud->getEstado() . "');";
 
         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    public function editarSolicitud($solicitud) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbsolicitud SET  tbsolicitudnumeroorden='" . $solicitud->getNumeroOrden() . 
                "', tbsolicitudnumeroabonado='" . $solicitud->getNumeroAbonado() .
                "', tbsolicitudfechasolicitud='" . $solicitud->getFechaSolicitud() .
                "', tbsolicitudestado='" . $solicitud->getEstado() .
                "' WHERE tbsolicitudid=" .$solicitud->getId() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function eliminarSolicitud($id) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $queryUpdate = "DELETE from tbsolicitud where tbsolicitudid=" . $id . ";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getSolicitudes() {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbsolicitud;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $clientes = [];
        while ($row = mysqli_fetch_array($result)) {
            $clienteActual = new solicitud($row['tbsolicitudid'],$row['tbsolicitudnumeroabonado'], $row['tbsolicitudnumeroorden'], $row['tbsolicitudfechasolicitud'], $row['tbsolicitudestado']);
            array_push($clientes, $clienteActual);
        }
        return $clientes;
    }



}
