<?php

include_once 'conexion.php';
include '../dominio/canal.php';

class ConsultasCanal extends Conexion {
    public function insertCanal($canal) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

         //Obtiene el ultimo id
         $queryGetLastId = "SELECT MAX(tbcanalid) AS tbcanalid  FROM tbcanal";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
 
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         
         printf($nextId);
         $queryInsert = "INSERT INTO tbcanal(tbcanalid,tbcanalnombre,tbcanalnumero,tbcanalicono,
                tbcanalsubtitulos,tbcanalidioma,tbcanalcategoria,tbcanalpais,tbcanalestado) VALUES ('" . $nextId . "','" .
                 $canal->getNombre() . "','" .
                 $canal->getNumero() . "','" .
                 $canal->getIcono() . "','" .
                 $canal->getSubtitulos() . "','" .
                 $canal->getIdioma() . "','" .
                 $canal->getCategoria() . "','" .
                 $canal->getPais() .    "','" .
                 $canal->getEstado() . "');";
 
         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    public function modificar($canal) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $queryUpdate = "UPDATE tbcanal SET  tbcanalnombre='" . $canal->getNombre() . 
                "', tbcanalnumero='" . $canal->getNumero() .
                "', tbcanalicono='" . $canal->getIcono() .
                "',tbcanalsubtitulos='" . $canal->getSubtitulos() .
                "',tbcanalidioma='" . $canal->getIdioma() .
                "',tbcanalcategoria='" . $canal->getCategoria() .
                "',tbcanalpais='" . $canal->getPais() .
                "',tbcanalestado='" . $canal->getEstado() .
                "' WHERE tbcanalid=" .$canal->getIdCanal() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function eliminarCable($canal) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $conn->set_charset('utf8');

        $queryUpdate = "UPDATE tbcanal SET  tbcanalestado='" . 0 .
                "' WHERE tbcanalid=" .$canal . ";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getCanales() {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbcanal WHERE tbcanalestado = '1'";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $canales = [];
        while ($row = mysqli_fetch_array($result)) {
            if($row['tbcanalsubtitulos']==1){
                $canalActual = new canal($row['tbcanalid'], $row['tbcanalnombre'], $row['tbcanalnumero'], $row['tbcanalicono'], 
                    "si", $row['tbcanalidioma'], $row['tbcanalcategoria'], $row['tbcanalpais'], $row['tbcanalestado']);
            }else{
                $canalActual = new canal($row['tbcanalid'], $row['tbcanalnombre'], $row['tbcanalnumero'], $row['tbcanalicono'], 
                    "No", $row['tbcanalidioma'], $row['tbcanalcategoria'], $row['tbcanalpais'], $row['tbcanalestado']);
            }
           
            array_push($canales, $canalActual);
        }
        return $canales;
    }
    

}
