<?php

include_once 'conexion.php';
include '../dominio/asignacionTecnico.php';


class ConsultasAsignacionTecnico extends Conexion {

    public function insertarAsignacionTecnico($asignacionTecnico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

         //Obtiene el ultimo id
         $queryGetLastId = "SELECT MAX(tbasignaciontecnicoid) AS tbasignaciontecnicoid  FROM tbasignaciontecnico";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         $estado = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         $queryInsert = "INSERT INTO tbasignaciontecnico(tbasignaciontecnicoid,tbasignaciontecnicoidsolicitud,tbasignaciontecniconombretecnico,
            	tbasignaciontecnicotipotrabajo, tbasignaciontecnicoestado) VALUES ('" . $nextId . "','" .
                $asignacionTecnico->getIdSolicitud() . "','" .
                $asignacionTecnico->getNombreTecnico() . "','" .
                $asignacionTecnico->getTipoTrabajo() . "','" .
                $estado . "');";
 
         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    public function editarAsignacionTecnico($asignacionTecnico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbasignaciontecnico SET  tbasignaciontecnicotipotrabajo='" . $asignacionTecnico->getTipoTrabajo() .
                "' WHERE tbasignaciontecnicoid=" . $asignacionTecnico->getId() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function eliminarAsignacionTecnico($id) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $queryUpdate = "UPDATE tbasignaciontecnico SET  tbasignaciontecnicoestado='" . 0 . 
            "' WHERE tbasignaciontecnicoid=" . $id . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getAsignacionTecnico() {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbasignaciontecnico WHERE tbasignaciontecnicoestado=1;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);

        $asignaciones = [];
        while ($row = mysqli_fetch_array($result)) {
            $asignacionActual = new AsignacionTecnico($row['tbasignaciontecnicoid'],$row['tbasignaciontecnicoidsolicitud'],
                 $row['tbasignaciontecniconombretecnico'], $row['tbasignaciontecnicotipotrabajo']);
            array_push($asignaciones, $asignacionActual);
        }

        return $asignaciones;
    }



}
