<?php

include_once 'conexion.php';
include '../dominio/cliente.php';


class ConsultasCliente extends Conexion {

    public function insertarCliente($cliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

         //Obtiene el ultimo id
         $queryGetLastId = "SELECT MAX(tbclienteid) AS tbclienteid  FROM tbcliente";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         $queryInsert = "INSERT INTO tbcliente(tbclienteid,tbclienteidentificacion,tbclientenombre,tbclienteapellido,tbclientecodcliente,
                tbclientecontrasena,tbclienteusuario,tbclienterol,tbclienteestado) VALUES ('" . $nextId . "','" .
                $cliente->getIdentificacion() . "','" .
                $cliente->getNombre() . "','" .
                $cliente->getApellido() . "','" .
                $cliente->getCodCliente() . "','" .
                $cliente->getContrasena() . "','" .
                $cliente->getUsuario() . "','" .
                $cliente->getRol() . "','" .
                $cliente->getEstado() . "');";

         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    public function editarCliente($cliente) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbcliente SET  tbclientenombre='" . $cliente->getNombre() . 
                "', tbclienteidentificacion='" . $cliente->getIdentificacion() .
                "', tbclienteapellido='" . $cliente->getApellido() .
                "', tbclientecodcliente='" . $cliente->getCodCliente() .
                "',tbclientecontrasena='" . $cliente->getContrasena() .
                "',tbclienteusuario='" . $cliente->getUsuario() .
                "',tbclienterol='" . $cliente->getRol() .
                "',tbclienteestado='" . $cliente->getEstado() .
                "' WHERE tbclienteid=" .$cliente->getId() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function eliminarCliente($id) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $queryUpdate = "DELETE from tbcliente where tbclienteid=" . $id . ";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getClientes() {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbcliente;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $clientes = [];
        while ($row = mysqli_fetch_array($result)) {
            $clienteActual = new cliente($row['tbclienteid'],$row['tbclienteidentificacion'], $row['tbclientenombre'], $row['tbclienteapellido'], $row['tbclientecodcliente'], 
                    $row['tbclientecontrasena'], $row['tbclienteusuario'], $row['tbclienterol'],$row['tbclienteestado']);
            array_push($clientes, $clienteActual);
        }
        return $clientes;
    }

    //------------funcion para obtenr el ultimo id-------
    public function obtenerUltimoId(){
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

         //Obtiene el ultimo id
         $queryGetLastId = "SELECT MAX(tbclienteid) AS tbclienteid  FROM tbcliente";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]);
        }
        return $nextId;
    }
    

    public function inicioSesion($usuario,$contrasena){

        session_start();

        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

        $queryObtenerUsuario = "SELECT tbclienterol FROM tbcliente WHERE tbclienteusuario='$usuario' and tbclientecontrasena='$contrasena'";
        $result = mysqli_query($conn, $queryObtenerUsuario);
        $row=mysqli_fetch_row($result);
        $rol=0;
        $temp="";
        if (mysqli_num_rows($result)>0) {                 
            $rol=$row[0];
            $_SESSION['rol']=$rol;
            if ($rol=="Administrador") {
                $temp="admin";
            }else if ($rol=="Cliente"){
                $temp="cliente";
            }else if ($rol=="Tecnico"){
                $temp="Tecnico";
            }

        }
          
        mysqli_close($conn);
        return $temp;
    }

}
