<?php

    include_once 'conexion.php';
    include '../dominio/contacto.php';

    class ConsultasContactos extends Conexion {

        public function insertarContacto($ultimoIdCliente, $itemsTelefono, $itemsCorreo) {

            $result;

            $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
            $conn->set_charset('utf8');
            if(!$conn){
                printf('conexion fallida');
            }

            while(true){

                //Obtiene el ultimo id
                $queryUltimoId = "SELECT MAX(tbclientecontactoid) AS tbclientecontactoid  FROM tbclientecontacto";
                $idCont = mysqli_query($conn, $queryUltimoId);
                $siguienteId = 1;
                if ($row = mysqli_fetch_row($idCont)) {
                    $siguienteId = trim($row[0]) + 1;
                }

                //// RECUPERAR LOS VALORES DE LOS ARREGLOS ////////
				$itemTel = current($itemsTelefono);
                $itemCor = current($itemsCorreo);
                
                ////// ASIGNARLOS A VARIABLES ///////////////////
				$telefono=(( $itemTel !== false) ? $itemTel : ", &nbsp;");
                $correo=(( $itemCor !== false) ? $itemCor : ", &nbsp;");
                $estado = 1;
                
                //// CONCATENAR LOS VALORES EN ORDEN PARA SU FUTURA INSERCIÓN ////////
				$valores='('.$siguienteId.',"'.$ultimoIdCliente.'","'.$telefono.'","'.$correo.'","'.$estado.'"),';

				///// YA QUE TERMINA CON COMA CADA FILA, SE RESTA CON LA FUNCIÓN SUBSTR EN LA ULTIMA FILA /////////
                $valoresQ= substr($valores, 0, -1);
                
                // INSERCION A LA BASE DE DATOS//////
                $queryInsert = "INSERT INTO tbclientecontacto(tbclientecontactoid, tbclienteid, tbclientecontactotelefono, 
                        tbclientecontactocorreo,tbclientecontactoestado) VALUES $valoresQ";
 
                //$result = mysqli_query($conn, $queryInsert);

                ////////////////////////
                $sqlRes=$conn->query($queryInsert);


                // Up! Next Value
				$itemTel = next( $itemsTelefono );
				$itemCor = next( $itemsCorreo );
				    
				// Check terminator
                if($itemTel === false && $itemCor === false) 
                    break;
                    
            }
            return $sqlRes;
        }

        public function getContactoCliente($idCliente) {
        
            $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
            $conn->set_charset('utf8');
    
            $querySelect = "SELECT * FROM tbclientecontacto where tbclienteid=" . $idCliente . " AND  tbclientecontactoestado=1;";;
            $result = mysqli_query($conn, $querySelect);
            mysqli_close($conn);
            $contactos = [];
            while ($row = mysqli_fetch_array($result)) {
                $contactoActual = new Contacto($row['tbclientecontactoid'],$row['tbclienteid'], $row['tbclientecontactotelefono'], 
                    $row['tbclientecontactocorreo']);
                array_push($contactos, $contactoActual);
            }
            return $contactos;
        }

        public function editarContacto($contacto) {
            $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
            $conn->set_charset('utf8');
            $queryUpdate = "UPDATE tbclientecontacto SET tbclientecontactotelefono='" . $contacto->getTelefono() . 
                    "', tbclientecontactocorreo='" . $contacto->getCorreo() .
                    "' WHERE tbclientecontactoid=" .$contacto->getId() . ";";
    
            $result = mysqli_query($conn, $queryUpdate);
            mysqli_close($conn);
    
            return $result;
        }

        public function eliminarContacto($id) {
            $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
    
            $queryUpdate = "UPDATE tbclientecontacto SET tbclientecontactoestado=0 where tbclientecontactoid=" . $id . ";";
            $result = mysqli_query($conn, $queryUpdate);
            mysqli_close($conn);
    
            return $result;
        }
    }
?>
