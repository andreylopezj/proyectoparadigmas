<?php

include_once 'conexion.php';
include '../dominio/tecnico.php';

class ConsultasTecnico extends Conexion {

    public function insertar($tecnico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }
         //Obtiene el ultimoid+1 de tbtecnico
         $queryGetLastId = "SELECT MAX(tbtecnicoid) AS tbtecnicoid  FROM tbtecnico";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         //Registra en db
         $queryInsert = "INSERT INTO tbtecnico(tbtecnicoid,tbtecnicocedula,tbtecniconombre,tbtecnicoapellidos,
         tbtecnicousuario,tbtecnicocontrasenia,tbtecnicotelefono,tbtecnicocorreo,tbtecnicoestado) VALUES ('".$nextId."','".
         $tecnico->getCedula()."','".
         $tecnico->getNombre()."','".
         $tecnico->getApellidos()."','".
         $tecnico->getUsuario()."','".
         $tecnico->getContrasenia()."','".
         $tecnico->getTelefono()."','".
         $tecnico->getCorreo()."','1');";
         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    public function editar($tecnico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbtecnico SET  tbtecnicocedula='".$tecnico->getCedula(). 
                "',tbtecniconombre='" . $tecnico->getNombre() .
                "',tbtecnicoapellidos='" . $tecnico->getApellidos() .
                "',tbtecnicousuario='" . $tecnico->getUsuario() .
                "',tbtecnicocontrasenia='" . $tecnico->getContrasenia() .
                "',tbtecnicotelefono='" . $tecnico->getTelefono() .
                "',tbtecnicocorreo='" . $tecnico->getCorreo() .
                "' WHERE tbtecnicoid=" .$tecnico->getId() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function eliminar($id) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $queryUpdate = "UPDATE tbtecnico SET  tbtecnicoestado='" . 0 .
                "' WHERE tbtecnicoid=" .$id. ";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getTecnicos() {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbtecnico where tbtecnicoestado=1;";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $tecnicos = [];
        while ($row = mysqli_fetch_array($result)) {
            $tecnicoActual = new Tecnico($row['tbtecnicoid'],$row['tbtecnicocedula'], $row['tbtecniconombre'], 
            $row['tbtecnicoapellidos'],$row['tbtecnicousuario'],$row['tbtecnicocontrasenia'],$row['tbtecnicotelefono'],
            $row['tbtecnicocorreo']);
            array_push($tecnicos, $tecnicoActual);
        }
        return $tecnicos;
    }
    

}
