<?php

include_once 'conexion.php';
include '../dominio/direccion.php';

class ConsultasDireccion extends Conexion {

    public function insertDireccion($direccion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }
         //Obtiene el ultimoid+1 de descripciones
         $queryGetLastId = "SELECT MAX(tbdireccionid) AS tbdireccionid  FROM tbdireccion";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         //Obtiene el ultimoid+1 de clientes 
         $queryGetLastIdCliente = "SELECT MAX(tbclienteid) AS tbclienteid  FROM tbcliente";
         $idClienteCont = mysqli_query($conn, $queryGetLastIdCliente);
         $nextIdCliente = 1;
         if ($row = mysqli_fetch_row($idClienteCont)) {
             $nextIdCliente = trim($row[0]) + 1;
         };
         //Registra en db
         $queryInsert = "INSERT INTO tbdireccion(tbdireccionid,tbdireccionidcliente,tbdireccionprovincia,tbdireccioncanton,
         tbdirecciondistrito,tbdireccionexacta) VALUES ('".$nextId."','".$nextIdCliente."','".
         $direccion->getProvincia()."','".
         $direccion->getCanton()."','".
         $direccion->getDistrito()."','".
         $direccion->getExacta()."');";
         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    public function modificar($direccion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbdireccion SET  tbdireccionprovincia='" . $direccion->getProvincia() . 
                "', tbdireccioncanton='" . $direccion->getCanton() .
                "', tbdirecciondistrito='" . $direccion->getDistrito() .
                "',tbdireccionexacta='" . $direccion->getExacta() .
                "' WHERE tbdireccionid=" .$direccion->getId() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function eliminarDireccion($idDireccion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $queryUpdate = "DELETE from tbdireccion where tbdireccionid=" . $idDireccion . ";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getDirecciones($idCliente) {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbdireccion where tbdireccionidcliente=".$idCliente.";";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $direcciones = [];
        while ($row = mysqli_fetch_array($result)) {
            $direccionActual = new Direccion($row['tbdireccionid'],$row['tbdireccionprovincia'], $row['tbdireccioncanton'], 
            $row['tbdirecciondistrito'],$row['tbdireccionexacta']);
            array_push($direcciones, $direccionActual);
        }
        return $direcciones;
    }
    

}
