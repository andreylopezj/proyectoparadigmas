<?php 
include_once 'conexion.php';
include '../dominio/peticionTecnico.php';


class consultasPeticionTecnico extends Conexion {

    public function insertar($peticionTecnico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        if(!$conn){
            printf('conexion fallida');
        }

         //Obtiene el ultimo id
         $queryGetLastId = "SELECT MAX(tbatenciontecnicoid) AS tbatenciontecnicoid  FROM tbatenciontecnico";
         $idCont = mysqli_query($conn, $queryGetLastId);
         $nextId = 1;
         if ($row = mysqli_fetch_row($idCont)) {
             $nextId = trim($row[0]) + 1;
         }
         $queryInsert = "INSERT INTO tbatenciontecnico(tbatenciontecnicoid,tbatenciontecnicofecha,tbatenciontecnicocambioequipo,
         tbatenciontecnicodescripcion,tbatenciontecnicoidtecnico,tbatenciontecnicoestado) VALUES ('" . $nextId . "','" .
                $peticionTecnico->getFecha() . "','" .
                $peticionTecnico->getCambioEquipo() . "','" .
                $peticionTecnico->getDescripcion() . "','" .
                $peticionTecnico->getIdTecnico() . "','" .
                $peticionTecnico->getEstado() . "');";

         $result = mysqli_query($conn, $queryInsert);
         mysqli_close($conn);
         return $result;
    }

    
    public function eliminar($peticion) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);

        $conn->set_charset('utf8');

        $queryUpdate = "UPDATE tbatenciontecnico SET  tbatenciontecnicoestado='" . 0 .
                "' WHERE tbatenciontecnicoid=" .$peticion . ";";
        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function modificar($peticionTecnico) {
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');
        $queryUpdate = "UPDATE tbatenciontecnico SET  tbatenciontecnicofecha='" . $peticionTecnico->getFecha() . 
                "', tbatenciontecnicocambioequipo='" . $peticionTecnico->getCambioEquipo() .
                "', tbatenciontecnicodescripcion='" . $peticionTecnico->getDescripcion() .
                "', tbatenciontecnicoidtecnico='" . $peticionTecnico->getIdTecnico() . 
                "' WHERE tbatenciontecnicoid=".$peticionTecnico->getId() . ";";

        $result = mysqli_query($conn, $queryUpdate);
        mysqli_close($conn);

        return $result;
    }

    public function getPeticionTecnico() {
        
        $conn = mysqli_connect($this->server, $this->user, $this->password, $this->db);
        $conn->set_charset('utf8');

        $querySelect = "SELECT * FROM tbatenciontecnico WHERE tbatenciontecnicoestado = '1'";
        $result = mysqli_query($conn, $querySelect);
        mysqli_close($conn);
        $peticion = [];
        while ($row = mysqli_fetch_array($result)) {
                $actual = new peticionTecnico($row['tbatenciontecnicoid'], $row['tbatenciontecnicofecha'], $row['tbatenciontecnicocambioequipo'], $row['tbatenciontecnicodescripcion']
                    , $row['tbatenciontecnicoidtecnico'],$row['tbatenciontecnicoestado']);
            
           
            array_push($peticion, $actual);
        }
        return $peticion;
    }
}