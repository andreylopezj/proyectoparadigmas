<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Cliente"||$_SESSION['rol'] == "Tecnico"||$_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }

?>

<!DOCTYPE html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>canal crud cliente</title>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/direcion.js"></script>

    <?php
    include '../negocio/clienteNegocio.php';
    ?>

    <script>			
        $(function(){
		    // Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
		    $("#adicional").on('click', function(){
			    $("#tabla tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tabla");
		    });
			 
		    // Evento que selecciona la fila y la elimina 
		    $(document).on("click",".eliminar",function(){
			    var parent = $(this).parents().get(0);
			    $(parent).remove();
		    });
	    });
	</script>

</head>

<body>

    <nav>
        <ul>
            <li><a href="../vista/inicio.php">Inicio</a></li>
        </ul>
    </nav>

    <h3>Registro Cliente</h3>

    <form action="../negocio/clienteAccion.php" method="post" onclick="registrarDireccion();">
        <label id="identificacion">Identicación :</label>
        <input type="number" name="identificacion" />
        <br><br>
        <label id="nombre">Nombre</label>
        <input type="text" name="nombre" />
        <br><br>
        <label id="ape">Apellido</label>
        <input type="text" name="apellido" />
        <br><br>                
        <label id="direccion">Usuario</label>
        <input type="text" name="usuario" />
        <br><br>
        <label id="direccion">Contrasena</label>
        <input type="text" name="contrasena" />
        <br><br>
        <label id="direccion">Rol</label>
        <select name="rol">
                    <option>Administrador</option>
                    <option>Tecnico</option>
                    <option>Cliente</option>
                    <option>Oficina</option>
        </select>
        <br><br>
        <!--///////////Registro de contacto///////////////-->
        <label id="">contactos del cliente</label>
        <br>
        <table   id="tabla">
			<tr class="fila-fija">
				<td><input required name="telefono[]" placeholder="Telefono"/></td>
				<td><input required name="correo[]" placeholder="Correo Electronico"/></td>
				<td class="eliminar"><input type="button"   value="Menos -"/></td>
			</tr>
		</table>
                
        <div class="btn-der">
			<button id="adicional" name="adicional" type="button" class="btn btn-warning"> Más + </button>
		</div>
        <br>
        
        <!--///////////Registro de Direccion///////////////-->
        <label id="">Direccion del cliente</label>
        <br>
    <label id="Direccion">Direccion: </label> <select id="provincia" onchange="updateDistrito(1);updateCanton(1);"></select>
    <label id="Direccion">Canton: </label> <select id="canton" onchange="updateDistrito(1);"></select>
    <label id="Direccion">Distrito: </label> <select id="distrito"></select>
    <br>
    <label id="Direccion">Direccion Exacta: <textarea id="exacta" name="exacta" rows="5" cols="25"></textarea>
    <br>
    <input  style="background: #008f39;" type="button" value="Agregar direccion" name="Agregardireccion" id="Agregardireccion" onclick="guardarFila();"/>
    <br>
    <table id="tablaDireccion">
        <thead>
            <tr>
                <th>Provincia</strong></th><th><strong>Canton</strong></th><th><strong>Distrito</strong></th><th><strong>Exacta</strong></th><th><strong>Ación</strong></th>
            </tr>
        </thead>
        <tbody id="direcciones">
        </tbody>
    </table>
        <!--////////////////////////////-->
        <td><input  style="background: #008f39;" type="submit" value="Registrar" name="registrar" id="registrar"/></td>             
    </form>

    <br><br>

    <h3>Lista Canal</h3>
    <section id="form">
        <table border=1>
            <tr>
                <th>Identificacion</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>detalles</th>
                <th>Accion</th>
            </tr>
            <?php
            
                $clienteNegocio = new ClienteNegocio();
                $clientes = $clienteNegocio->getClientes();
                foreach ($clientes as $current) {
                    $idcliente=$current->getId();
                    $nombre=$current->getNombre();
                    $apellido=$current->getApellido();
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/clienteAccion.php">';
                    echo '<input type="hidden" name="id" id="id" value="' . $current->getId() . '">';
                    echo '<tr>';
                    echo '<td><input type="text" name="identificacion" id="identificacion" value="' . $current->getIdentificacion() . '"/></td>';
                    echo '<td><input type="text" name="nombre" id="nombre" value="' . $current->getNombre() . '"/></td>';
                    echo '<td><input type="text" name="apellido" id="apellido" value="' . $current->getApellido() . '"/></td>';
                
                    echo "<td><a href='contactoclientevista.php?id=$idcliente&nom=$nombre&ape=$apellido'>Ver Contactos</a></td>";
                    
                    echo '<td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                          <input type="submit" style="background: #75B1F2;" value="Editar" name="editar" id="editar"/></td>';
                    echo '</tr>';
                    echo '</form>';
                }
            ?>
        </table>

        <?php
            if (isset($_GET['error'])) {
                if ($_GET['error'] == "emptyField") {
                    echo '<p style="color: red">Campo(s) vacio(s)</p>';
                } else if ($_GET['error'] == "numberFormat") {
                    echo '<p style="color: red">Error, formato de Apellido</p>';
                } else if ($_GET['error'] == "dbError") {
                    echo '<center><p style="color: red">Error al procesar la transacción</p></center>';
                }
            } else if (isset($_GET['success'])) {
                echo '<p style="color: green">Se realizadó correctamente</p>';
            }
        ?>

    </section>
    

    <footer>

    </footer>
    

  

</body>
</html>