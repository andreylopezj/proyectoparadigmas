<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Tecnico" || $_SESSION['rol'] == "Cliente" || $_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro Asignacion Tecnico</title>

    <?php
        include '../negocio/asignacionTecnicoNegocio.php';
        include '../negocio/solicitudNegocio.php';
        include '../negocio/tecnicoNegocio.php';
    ?>

</head>

<body>
    <nav>
        <ul>
            <li><a href="../vista/inicio.php">Inicio</a></li>
        </ul>
    </nav>

    <h3>Registro de Asignacion Tecnico</h3>

    <form action="../negocio/asignacionTecnicoAccion.php" method="post">
        Numero Abonado: <br/>
        <select name="idsolicitud" id="idsolicitud">
            <?php
                $solicitudNegocio = new SolicitudNegocio();
                $solicitudes = $solicitudNegocio->getSolicitudes();
                foreach ($solicitudes as $solicitud) {
            ?> 
            <option value=" <?php echo $solicitud->getId() ?> "> <?php echo $solicitud->getNumeroAbonado() ?> </option>
            <?php
                }
            ?>            
        </select>        
        <br/><br/>     
        
        Nombre Tecnico: <br/>
        <select name="nombre" id="nombre">
            <?php
                $tecnicoNegocio = new TecnicoNegocio();
                $tecnicos = $tecnicoNegocio->getTecnicos();
                foreach ($tecnicos as $tecnico) {                    
                $nom = $tecnico->getNombre();
                $apell = $tecnico->getApellidos();
            ?> 
            <option value=" <?php echo $nom," ",$apell ?> "> <?php echo $nom," ",$apell ?> </option>
            <?php
                }
            ?>            
        </select>  
        <br/><br/>

        Tipo de Trabajo: <br/><input type="text" name="tipoTrabajo" id="tipoTrabajo"><br/><br/>
        <input  style="background: #008f39;" type="submit" value="Registrar" name="registrar" id="registrar"/>  
    </form>

    <br><br>

    <h3>Lista de Asignaciones</h3>
    <table border=1>
        <tr>
            <th>Id de solicitud</th>
            <th>Nombre Tecnico</th>
            <th>tipo Trabajo</th>
            <th>Acciones</th>
        </tr>
        <?php
            $asignacionTecnicoNegocio = new AsignacionTecnicoNegocio();
            $asignaciones = $asignacionTecnicoNegocio->getAsignacionTecnico();
            
            foreach ($asignaciones as $asignacion) {
                echo '<form method="post" action="../negocio/asignacionTecnicoAccion.php">';
                echo '<input type="hidden" name="id" id="id" value="' . $asignacion->getId() . '">';
                echo '<tr>';
                echo '<td><input type="text" name="solicitudId" id="solicitudId" disabled="disabled" value="' . $asignacion->getIdSolicitud() . '"/></td>';
                echo '<td><input type="text" name="nombreTecnico" id="nombreTecnico" disabled="disabled" value="' . $asignacion->getNombreTecnico() . '" /></td>';
                echo '<td><input type="text" name="trabajoTipo" id="trabajoTipo" value="' . $asignacion->getTipoTrabajo() . '"/></td>';
                echo '<td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                          <input type="submit" style="background: #75B1F2;" value="Editar" name="editar" id="editar"/></td>';
                echo '</tr>';
                echo '</form>'; 
            }
        ?>
    </table>

    <?php
        if (isset($_GET['error'])) {
            if ($_GET['error'] == "campos") {
                echo '<p style="color: red">Campo(s) vacio(s)</p>';
            }else if ($_GET['error'] == "dbError") {
                echo '<center><p style="color: red">Error en la consulta</p></center>';
            }else if($_GET['error'] == "formatoPos"){
                echo '<center><p style="color: red">Numero Abonado y/o Numero Orden deben ser numeros positivos</p></center>';
            }else if($_GET['error'] == "iguales"){
                echo '<center><p style="color: red">Numero Orden invalido</p></center>';
            }
        } else if (isset($_GET['exito'])) {
            if ($_GET['exito'] == "inserto") {
                echo '<p style="color: green">Registrado!</p>';
            }elseif ($_GET['exito'] == "elimino") {
                echo '<p style="color: green">Se ha eliminado Correctmente</p>';
            }elseif ($_GET['exito'] == "edito") {
                echo '<p style="color: green">Cambios guardados</p>';
            }
        }
    ?>
</body>
</html>