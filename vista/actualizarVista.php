<!DOCTYPE html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>canal crud</title>
    <link rel="icon" href="../resources/icons/bull.png">
    <link rel="stylesheet" href="../resources/css/css.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <?php
    include '../negocio/canalNegocio.php';
    ?>

</head>

<body>

    <header> 
    </header>

    <?php
        
    ?>

    <nav>
        <ul>
            <li><a href="../prueba.php">Inicio</a></li>
        </ul>
    </nav>

    <br>
    <h3>Registro Canal</h3>

<form action="../negocio/canalAcciones.php" method="post">
                <label id="nombre">Nombre</label>
                <input type="text" name="nombre" />
                <br><br>
                <label id="numero">Número</label>
                <input type="number" name="numero" id="time" value="0" style="width: 150px;" />
                <br><br>
                <label id="icono">Icono</label>
                <input class="input-group" type="file" name="icono" accept="image/*" />
                <br><br>
                <label id="subtitulos">Subtitulos</label>
                <select name="subtitulos">
                    <option>Si</option>
                    <option>No</option>
                </select>   
                <br><br>
                <label id="idioma">Idioma</label>
                <input type="text" name="idioma"/>
                <br><br>
                <label id="categoria">Categoría</label>
                <select name="categoria">
                    <option>Internacionales</option>
                    <option>Documentales</option>
                    <option>Infantíles</option>
                    <option>Deportes</option>
                    <option>Musicales</option>
                    <option>Cine</option>
                    <option>Series y entretenimiento</option>
                    <option>Información</option>
                    <option>Radio</option>
                    <option>Religiosos</option>
                    <option>Otros</option>
                </select>
                <br><br>
                <label id="pais">País</label>
                <select name="pais">
                    <option>Costa Rica</option>
                    <option>Mexico</option>
                    <option>Nicaragua</option>
                    <option>Colombia</option>
                    <option>Estados Unidos</option>
                    <option>España</option>
                    <option>Honduras</option>
                    <option>Salvador</option>
                    <option>Brasil</option>
                    <option>Inglaterra</option>
                    <option>Venezuela</option>
                    <option>Chile</option>
                    <option>Francia</option>
                    <option>Italia</option>
                    <option>Argentina</option>
                    <option>Uruguay</option>
                </select> 
                <br><br>
                <td><input type="submit" value="Crear" name="create" id="create"/></td>             
    </form>

    </body>
</html>