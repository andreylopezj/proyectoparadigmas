<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Administrador"||$_SESSION['rol'] == "Cliente" || $_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crud Tecnico</title>
    <?php
        include '../negocio/tecnicoNegocio.php';
    ?>
</head>
<body>
    <h1>Tecnico</h1>
    <a href="../datos/cerrarSesion.php">Cerrar Sesion</a>

    <h2>Crud Tecnico</h1>

    <h3>Registro de Tenicos</h2>
    <form method="Post" action="../negocio/tecnicoAcciones.php">
        <label id="labelCedula">Cedula:</label>
        <input type="text" id="cedula" name="cedula">
        <br><br> 
        <label id="labelNombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre">
        <br><br> 
        <label id="labelApellidos">Apellidos:</label>
        <input type="text" id="apellidos" name="apellidos">
        <br><br> 
        <label id="labelUsuario">Usuario:</label>
        <input type="text" id="usuario" name="usuario">
        <br><br> 
        <label id="labelContraseña">Contraseña:</label>
        <input type="password" id="contrasenia" name="contrasenia">
        <br><br> 
        <label id="labelTelefono">Telefono:</label>
        <input type="text" id="telefono" name="telefono">
        <br><br> 
        <label id="labelCorreo">Correo:</label>
        <input type="text" id="correo" name="correo">
        <br><br>
        <td><input  style="background: #008f39;" type="submit" value="Registrar" name="registrar" id="registrar"/></td>
    </form>

    <h3>Lista de Tecnicos</h3>
    <table id="tablaTecnicos">
        <thead>
            <tr>
                <th><strong>Nombre</strong></th>
                <th><strong>Apellidos</strong></th>
                <th><strong>Usuario</strong></th>
                <th><strong>Contraseña</strong></th>
                <th><strong>Telefono</strong></th>
                <th><strong>Correo</strong></th>
            </tr>
        </thead>
        <tbody id="direcciones">
        <?php
            
            $tecnicoNegocio = new TecnicoNegocio();
            $tecnicos = $tecnicoNegocio->getTecnicos();
            foreach ($tecnicos as $tecnicoActual) {
                $id=$tecnicoActual->getId();
                $cedula=$tecnicoActual->getCedula();
                $nombre=$tecnicoActual->getNombre();
                $apellidos=$tecnicoActual->getApellidos();
                $usuario=$tecnicoActual->getUsuario();
                $contrasenia=$tecnicoActual->getContrasenia();
                $telefono=$tecnicoActual->getTelefono();
                $correo=$tecnicoActual->getCorreo();
                echo ('<form method="post" enctype="multipart/form-data" action="../negocio/tecnicoAcciones.php">
                <tr>
                <input type="hidden" id="id" name="id" value="'.$id.'"/>
                <input type="hidden" name="cedula" id="cedula" value="'.$cedula.'"/>
                <td><input type="text" name="nombre" id="nombre" value="'.$nombre.'"/></td>
                <td><input type="text" name="apellidos" id="apellidos" value="'.$apellidos.'"/></td>
                <td><input type="text" name="usuario" id="usuario" value="'.$usuario.'"/></td>
                <td><input type="text" name="contrasenia" id="contrasenia" value="'.$contrasenia.'"/></td>
                <td><input type="text" name="telefono" id="telefono" value="'.$telefono.'"/></td>
                <td><input type="text" name="correo" id="correo" value="'.$correo.'"/></td>
                <td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                <input type="submit" style="background: #75B1F2;" value="Editar" name="editar" id="editar"/></td>
                </tr>
                </form>');
            }
        ?>
        <tr>
                <td></td>
                <td>
                    <?php
                    if (isset($_GET['error'])) {
                        if ($_GET['error'] == "emptyField") {
                            echo '<p style="color: red">Campo(s) vacio(s)</p>';
                        } else if ($_GET['error'] == "numberFormat") {
                            echo '<p style="color: red">Error, formato de numero</p>';
                        } else if ($_GET['error'] == "dbError") {
                            echo '<center><p style="color: red">Error al procesar la transacción</p></center>';
                        }
                    } else if (isset($_GET['success'])) {
                        echo '<p style="color: green">Se realizadó correctamente</p>';
                    }
                    ?>
                </td>
            </tr>
        </tbody>
    </table>

</body>
</html>