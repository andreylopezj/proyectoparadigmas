<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Cliente"||$_SESSION['rol'] == "Tecnico"||$_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }


?>

<html>
	<head>
		<title>Administrar Canal</title>
	</head>	
	<body>
		<header>
			Bienvenido Administrar Canal
		</header>
		<table border=1>			
			<tr>
				<td><a href="../vista/canalVista.php">Registrar Canal</a></td>
			</tr>
			<tr>
				<td><a href="../vista/clienteVista.php">Registrar Cliente</a></td>
			</tr>
			<tr>
				<td><a href="../vista/asignacionTecnicoVista.php">Asignar Tecnico a Mantenimiento</a></td>
			</tr>
			<tr>
				<td><a href="../vista/tecnicoVista.php">Registrar Tecnico</a></td>
			</tr>
			<tr>
				<td><a href="../vista/atencionPeticionTecnicoVista.php">Atencion de peticion</a></td>
			</tr>
			<tr>
				<td><a href="../datos/cerrarSesion.php">Cerrar Sesion</a></td>
			</tr>
		</table>

	</body>
</html>