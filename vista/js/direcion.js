
//Ingresa las provincias al select
$(document).ready(function ()  {
    $('#provincia').empty();
    $.ajax({
        dataType: "json",
        url: "https://ubicaciones.paginasweb.cr/provincias.json",
        data: {},
        success: function (data) {
            for(key in data) {
                $('#provincia').append(new Option(data[key],key));
            }
        }
    });
    updateCanton(0);
    updateDistrito(0);
  });


  //Ingresa los cantones al select
  function updateCanton(n){
    //var posicion;
        var val = document.getElementById('provincia');
        var posicion = $('#provincia option:selected').val();
        var enlace;
        if(n==0){
            enlace="https://ubicaciones.paginasweb.cr/provincia/1/cantones.json";
        }else{
            enlace="https://ubicaciones.paginasweb.cr/provincia/"+posicion+"/cantones.json";
        }  
    $.ajax({
        dataType: "json",
        url: enlace,
        data: {},
        success: function (data) {
        var html;
        for(key in data) {
            html += "<option value='"+key+"'>"+data[key]+"</option>";
        }
            $('#canton').html(html);
        }
    });
    
  }

  //Ingresa los distritos al select
  function updateDistrito(n){
    var val = document.getElementById('provincia');
    var provinciaPosicion = $('#provincia option:selected').val();
    var val2 = document.getElementById('canton');
    var cantonPosicion = $('#canton option:selected').val();
    var enlace;
    if(n==0){
        enlace = "https://ubicaciones.paginasweb.cr/provincia/1/canton/1/distritos.json";
    }else{
        enlace = "https://ubicaciones.paginasweb.cr/provincia/"+provinciaPosicion+"/canton/"+cantonPosicion+"/distritos.json";
    }
    $.ajax({
        dataType: "json",
        url: enlace,
        data: {},
        success: function (data) {
        var html;
        for(key in data) {
            html += "<option value='"+key+"'>"+data[key]+"</option>";
        }
            $('#distrito').html(html);
        }
    });

  }

//Guarda la fila en la tabla
function guardarFila(){
    //Captura de datos
    var _provincia = $('#provincia option:selected').text();
    var _canton = $('#canton option:selected').text();
    var _distrito = $('#distrito option:selected').text();
    var _exacta = document.getElementById("exacta").value;
    document.getElementById('exacta').value = "";
    //Creacion de fila
    var fila="<tr><td>"+_provincia+"</td><td>"+_canton+"</td><td>"+_distrito+"</td><td>"+_exacta+"</td><td><input  style='background: #B22222;' type='button' class='delete' value='Eliminar'/></td></tr>";
    var btn = document.createElement("TR");
   	btn.innerHTML=fila;
    document.getElementById("direcciones").appendChild(btn);
}

//Elimina la fila selecionada
$(document).on('click', '.delete', function() {
    $(this).parents('tr').remove();  
 });

//Envia el contenido de la tabla al backend 
  function registrarDireccion(){
    jQuery('#registrar').on('click', function() {
        var filas = [];
        $('#tablaDireccion tbody tr').each(function() {
          var provincia = $(this).find('td').eq(0).text();
          var canton = $(this).find('td').eq(1).text();
          var distrito = $(this).find('td').eq(2).text();
          var exacta = $(this).find('td').eq(3).text();
          var fila = {
              provincia,
              canton,
              distrito,
              exacta
          };
          filas.push(fila);
        });
        console.log(filas);
        $.ajax({
          type: "POST",
          url: "http://localhost/proyectoparadigmas/negocio/direccionAcciones.php",
          data: {valores : JSON.stringify(filas) },
          success: function(data) { 
             console.log(data);
          }
        });
      });
  }