<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Cliente" || $_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }

?>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Registro Solicitud</title>
    <?php
    include '../negocio/peticionTecnicoNegocio.php';
    ?>
</head>
<body>
    <nav>
        <ul>
            <li><a href="../vista/inicio.php">Inicio</a></li>
        </ul>
    </nav>
        <h3>Solicitudes de mantenimiento</h3>


        <form action="../negocio/peticionTecnicoAcciones.php" method="post">


        <label id="fechaC">Fecha de terminado</label>
        <br/><input type="date" name="fechaCreacion" id="fechaCreacion"><br/><br/>
        <label id="cambioE">Se realiazo cambio de equipo</label>
        <input type="checkbox" name="cambioEquipo" id="cambioEquipo"><br/><br/>
        <label id="descrip">Descripcion del trabajo</label><br/>
        <input type="text" id="descripcion" name="descripcion"/> <br/><br/>
        <label id="idTecn">Nombre del tecnico</label>
        <input type="text" id="idTecnico" name="idTecnico"/> <br/>
        <td><input type="submit" value="crear" name="crear" id="crear"/></td>  
        </form>

        <section id="form">
        <h3>Lista de reportes</h3>

        <table>
            <tr>
             
                <th>Fecha de creacion</th>
                <th>Cambio de equipo</th>
                <th>Descripcion</th>
                <th>Id del tecnico</th>
                

                <th>Accion</th>
            </tr>
            <?php
                $peticionTecnicoNegocio = new peticionTecnicoNegocio();
                $peticionTecnico = $peticionTecnicoNegocio->getPeticionTecnico();
                foreach ($peticionTecnico as $current) {
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/peticionTecnicoAcciones.php">';
                    echo '<input type="hidden" name="id" id="id" value="' . $current->getId() . '">';
                    echo '<tr>';
                    echo '<td><input type="text" name="fechaCreacion" id="fechaCreacion" value="' . $current->getFecha() . '"/></td>';
                    echo '<td><input type="text" name="cambioEquipo" id="cambioEquipo" value="' . $current->getCambioEquipo() . '"/></td>';
                    echo '<td><input type="text" name="descripcion" id="descripcion" value="' . $current->getDescripcion() . '"/></td>';
                    echo '<td><input type="text" name="idTecnico" id="idTecnico" value="' . $current->getIdTecnico() . '"/></td>';
                    echo '<td><input type="hidden" name="estado" id="estado" value="' . $current->getEstado() . '"/></td>';
                    echo '<td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                    <input type="submit"  style="background: #75B1F2;" value="Actualizar" name="modificar" id="modificar"/</td>';
                   
                    echo '</tr>';
                    echo '</form>';
                }
            ?>


            <tr>
                <td></td>
                <td>
                    <?php
                    if (isset($_GET['error'])) {
                        if ($_GET['error'] == "emptyField") {
                            echo '<p style="color: red">Campo(s) vacio(s)</p>';
                        } else if ($_GET['error'] == "numberFormat") {
                            echo '<p style="color: red">Error, formato de numero</p>';
                        } else if ($_GET['error'] == "dbError") {
                            echo '<center><p style="color: red">Error al procesar la transacción</p></center>';
                        }
                    } else if (isset($_GET['success'])) {
                        echo '<p style="color: green">Se realizadó correctamente</p>';
                    }
                    ?>
                </td>
            </tr>
        </table>
    </section>




    </body>
</html>