
<?php

session_start();

if(!isset($_SESSION['rol'])){
    header('location: ../vista/login.php');
}else{
    if($_SESSION['rol'] == "Cliente"||$_SESSION['rol'] == "Tecnico"||$_SESSION['rol'] == "Oficina"){
        header('location: ../vista/login.php');
    }
}


?>
<!DOCTYPE html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>canal crud</title>
    <link rel="icon" href="../resources/icons/bull.png">
    <link rel="stylesheet" href="../resources/css/css.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <?php
    include '../negocio/canalNegocio.php';
    ?>

</head>

<body>

    <header> 
    </header>

    <?php
        
    ?>

    <nav>
        <ul>
            <li><a href="../vista/inicio.php">Inicio</a></li>
        </ul>
    </nav>

    <br>
    <h3>Registro Canal</h3>
    <form action="../negocio/canalAcciones.php" method="post">
                <label id="nombre">Nombre</label>
                <input type="text" name="nombre" />
                <br><br>
                <label id="numero">Número</label>
                <input type="number" name="numero" id="time" value="0" style="width: 150px;" />
                <br><br>
                <label id="icono">Icono</label>
                <input class="input-group" type="file" name="icono" accept="image/*" />
                <br><br>
                <label id="subtitulos">Subtitulos</label>
                <select name="subtitulos">
                    <option>Si</option>
                    <option>No</option>
                </select>   
                <br><br>
                <label id="idioma">Idioma</label>
                <input type="text" name="idioma"/>
                <br><br>
                <label id="categoria">Categoría</label>
                <select name="categoria">
                    <option>Internacionales</option>
                    <option>Documentales</option>
                    <option>Infantíles</option>
                    <option>Deportes</option>
                    <option>Musicales</option>
                    <option>Cine</option>
                    <option>Series y entretenimiento</option>
                    <option>Información</option>
                    <option>Radio</option>
                    <option>Religiosos</option>
                    <option>Otros</option>
                </select>
                <br><br>
                <label id="pais">País</label>
                <select name="pais">
                    <option>Costa Rica</option>
                    <option>Mexico</option>
                    <option>Nicaragua</option>
                    <option>Colombia</option>
                    <option>Estados Unidos</option>
                    <option>España</option>
                    <option>Honduras</option>
                    <option>Salvador</option>
                    <option>Brasil</option>
                    <option>Inglaterra</option>
                    <option>Venezuela</option>
                    <option>Chile</option>
                    <option>Francia</option>
                    <option>Italia</option>
                    <option>Argentina</option>
                    <option>Uruguay</option>
                </select> 
                <br><br>
                <td><input type="submit" value="Crear" name="create" id="create"/></td>             
    </form>

    <br><br>
    <h3>Lista Canal</h3>
    <!--
    <table id="tabla" border="1">
        <th>Nombre</th>
        <th>numero</th>
        <th>icono</th>
        <th>subtitulo</th>
        <th>idioma</th>
        <th>categoria</th>
        <th>pais</th>
        <th>acciones</th>
        
            <?php
                $canalNeg = new canalNegocio();
                $canales = $canalNeg->getCanal();
                foreach ($canales as $actual) {
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/canalAcciones.php">';
                    echo '<input type="hidden" name="idCable" id="idCable" value="' . $actual->getIdCanal() . '">';
                    echo '<tr>';  
                    echo '<td><label>' . $actual->getNombre() . '</label></td>';
                    echo '<td><label>' . $actual->getNumero() . '</label></td>';
                    echo '<td><label>' . $actual->getIcono() . '</label></td>';
                    echo '<td><label>' . $actual->getSubtitulos() . '</label></td>';
                    echo '<td><label>' . $actual->getIdioma() . '</label></td>';
                    echo '<td><label>' . $actual->getCategoria() . '</label></td>';
                    echo '<td><label>' . $actual->getPais() . '</label></td>';
                    echo'<td><a href="actualizarVista.php">Modificar</a>';
                    echo '<td><input style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/></td>';
                    echo '</tr>';
                    echo '</form>';
                }
            ?>            
    </table>
<<<<<<< Updated upstream
!-->
<!--///////////////////////////////////////////////////////////!-->
    
    <section id="form">


<!--///////////////////////////////////////////////////////////!-->
    
    <section id="form">
        <h3>Ejemplo profe</h3>

        <table>
            <tr>
                <th>Nombre</th>
                <th>numero</th>
                <th>icono</th>
                <th>subtitulo</th>
                <th>idioma</th>
                <th>categoria</th>
                <th>pais</th>

                <th>Accion</th>
            </tr>
            <?php
                $canalNeg = new canalNegocio();
                $canales = $canalNeg->getCanal();
                foreach ($canales as $current) {
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/canalAcciones.php">';
                    echo '<input type="hidden" name="idcanal" id="idcanal" value="' . $current->getIdCanal() . '">';
                    echo '<tr>';
                    echo '<td><input type="text" name="nombre" id="nombre" value="' . $current->getNombre() . '"/></td>';
                    echo '<td><input type="text" name="numero" id="numero" value="' . $current->getNumero() . '"/></td>';
                    echo '<td><input type="text" name="icono" id="icono" value="' . $current->getIcono() . '"/></td>';
                    echo '<td><input type="text" name="subtitulos" id="subtitulos" value="' . $current->getSubtitulos() . '"/></td>';
                    echo '<td><input type="text" name="idioma" id="idioma" value="' . $current->getIdioma() . '"/></td>';
                    echo '<td><input type="text" name="categoria" id="categoria" value="' . $current->getCategoria() . '"/></td>';
                    echo '<td><input type="text" name="pais" id="pais" value="' . $current->getPais() . '"/></td>';
                    echo '<td><input type="hidden" name="estado" id="estado" value="' . $current->getEstado() . '"/></td>';
                    echo '<td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                    <input type="submit"  style="background: #75B1F2;" value="Actualizar" name="modificar" id="modificar"/</td>';
                   
                    echo '</tr>';
                    echo '</form>';
                }
            ?>


            <tr>
                <td></td>
                <td>
                    <?php
                    if (isset($_GET['error'])) {
                        if ($_GET['error'] == "emptyField") {
                            echo '<p style="color: red">Campo(s) vacio(s)</p>';
                        } else if ($_GET['error'] == "numberFormat") {
                            echo '<p style="color: red">Error, formato de numero</p>';
                        } else if ($_GET['error'] == "dbError") {
                            echo '<center><p style="color: red">Error al procesar la transacción</p></center>';
                        }
                    } else if (isset($_GET['success'])) {
                        echo '<p style="color: green">Se realizadó correctamente</p>';
                    }
                    ?>
                </td>
            </tr>
        </table>
    </section>
    

    <footer>

    </footer>
    

  

</body>
</html>