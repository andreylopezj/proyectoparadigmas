<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Cliente"||$_SESSION['rol'] == "Tecnico"||$_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }


?>

<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>contactos cliente</title>

    <?php
        include '../negocio/contactoNegocio.php';
        include '../negocio/direccionNegocio.php';
    ?>
</head>

<body>

    <nav>
        <ul>
            <li><a href="clienteVista.php">Atras</a></li>
        </ul>
    </nav>


    <?php 
        // obtengo los datos enviados desde la vista cliente
        $idCliente = $_GET['id'];
        $nomCliente = $_GET['nom'];
        $apeCliente = $_GET['ape'];
    ?>

    <h3>Lista de contactos de <?php echo $nomCliente," ", $apeCliente?> </h3>
    <br><br>

    <section id="form">
        <table border=1>
            <tr>
                <th>Telefono</th>
                <th>Correo Electronico</th>
                <th>Accion</th>
            </tr>

            <?php              

                $contactoNegocio = new ContactoNegocio();
                $contactos = $contactoNegocio->getContactoCliente($idCliente);

                foreach ($contactos as $current) {
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/contactoAccion.php">';
                    echo '<input type="hidden" name="idcontacto" id="idcontacto" value="' . $current->getId() . '">';
                    echo '<tr>';
                    echo '<td><input type="text" name="telefono" id="telefono" value="' . $current->getTelefono() . '"/></td>';
                    echo '<td><input type="text" name="correo" id="correo" value="' . $current->getCorreo() . '"/></td>';                    
                    echo '<td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                          <input type="submit" style="background: #75B1F2;" value="Editar" name="editar" id="editar"/></td>';
                    echo '</tr>';
                    echo '</form>';
                }
            ?>            
        </table>

        <?php
            if (isset($_GET['error'])) {
                if ($_GET['error'] == "emptyField") {
                    echo '<p style="color: red">Campo(s) vacio(s)</p>';
                } else if ($_GET['error'] == "numberFormat") {
                    echo '<p style="color: red">Error, formato de Apellido</p>';
                } else if ($_GET['error'] == "dbError") {
                    echo '<center><p style="color: red">Error al procesar la transacción</p></center>';
                }
            } else if (isset($_GET['success'])) {
                echo '<p style="color: green">Se realizadó correctamente</p>';
            }
        ?>
        
    </section>

    <input type="hidden" id="idCliente" name="idCliente" value="<?php echo $_GET["id"]; ?>"/>
   <h1>Cliente <?php echo $_GET["nom"]; ?> <?php echo $_GET["ape"]; ?></h1>

   <br>
   <h2>Lista de direcciones</h2>
   <table id="tablaDireccion">
        <thead>
            <tr>
                <th>Provincia</strong></th><th><strong>Canton</strong></th><th><strong>Distrito</strong></th><th><strong>Exacta</strong></th><th><strong>Ación</strong></th>
            </tr>
        </thead>
        <tbody id="direcciones">
        <?php
            
            $direccionNegocio = new direccionNegocio();
            $direcciones = $direccionNegocio->getDireccion($_GET["id"]);
            foreach ($direcciones as $direccionActual) {
                $id=$direccionActual->getId();
                $provincia=$direccionActual->getProvincia();
                $canton=$direccionActual->getCanton();
                $distrito=$direccionActual->getDistrito();
                $exacta=$direccionActual->getExacta();
                echo ('<form method="post" enctype="multipart/form-data" action="../negocio/direccionAcciones.php?nom='.$_GET["nom"].'&ape='.$_GET["ape"].'">
                <tr>
                <input type="hidden" id="idCliente" name="idCliente" value="'.$_GET["id"].'"/>
                <input type="hidden" id="idDireccion" name="idDireccion" value="'.$id.'"/>
                <td><input type="text" name="provincia" id="provincia" value="' . $provincia . '"/></td>
                <td><input type="text" name="canton" id="canton" value="' . $canton . '"/></td>
                <td><input type="text" name="distrito" id="distrito" value="' . $distrito . '"/></td>
                <td><input type="text" name="exacta" id="exacta" value="' . $exacta . '"/></td>
                <td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                <input type="submit" style="background: #75B1F2;" value="Editar" name="editar" id="editar"/></td>
                </tr>
                </form>');
            }
        ?>
        </tbody>
    </table>

    
</body>
</html>