<?php

    session_start();

    if(!isset($_SESSION['rol'])){
        header('location: ../vista/login.php');
    }else{
        if($_SESSION['rol'] == "Tecnico" || $_SESSION['rol'] == "Administrador" || $_SESSION['rol'] == "Oficina"){
            header('location: ../vista/login.php');
        }
    }


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro Solicitud</title>
    <?php
    include '../negocio/solicitudNegocio.php';
    ?>
</head>
<body>
    <nav>
        <ul>
            <li><a href="../vista/inicioCliente.php">Inicio</a></li>
        </ul>
    </nav>
    <h3>Registro Solicitud</h3>

    <form action="../negocio/solicitudAccion.php" method="post">
        Numero Abonado: <br/><input type="number" name="numeroAbonado" id="numeroAbonado" min="1"><br/><br/>
        Numero Orden: <br/><input type="number" name="numeroOrden" id="numeroOrden" min="1"><br/><br/>
        Fecha Solicitud: <br/><input type="date" name="fechaSolicitud" id="fechaSolicitud"><br/><br/>
        <input  style="background: #008f39;" type="submit" value="Registrar" name="registrar" id="registrar"/>  
    </form>
    <br><br>

    <h3>Lista Solicitudes</h3>
    <table border=1>
        <tr>
            <th>Numero Abonado</th>
            <th>Numero Orden</th>
            <th>Fecha de Solicitud</th>
            <th>Accion</th>
        </tr>
        <?php
            $solicitudNegocio = new SolicitudNegocio();
            $solicitudes = $solicitudNegocio->getSolicitudes();
            foreach ($solicitudes as $solicitud) {
                if ($solicitud->getId()==1){
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/solicitudAccion.php">';
                    echo '<input type="hidden" name="id" id="id" value="' . $solicitud->getId() . '">';
                    echo '<tr>';
                    echo '<td><input type="text" name="numeroAbonado" id="numeroAbonado" value="' . $solicitud->getNumeroAbonado() . '" readonly="readonly"/></td>';
                    echo '<td><input type="text" name="numeroOrden" id="numeroOrden" value="' . $solicitud->getNumeroOrden() . '" readonly="readonly"/></td>';
                    echo '<td><input type="text" name="fechaSolicitud" id="fechaSolicitud" value="' . $solicitud->getFechaSolicitud() . '" readonly="readonly"/></td>';
                    echo '</tr>';
                    echo '</form>';                    
                }elseif($solicitud->getEstado()==1){
                    echo '<form method="post" enctype="multipart/form-data" action="../negocio/solicitudAccion.php">';
                    echo '<input type="hidden" name="id" id="id" value="' . $solicitud->getId() . '">';
                    echo '<tr>';
                    echo '<td><input type="text" name="numeroAbonado" id="numeroAbonado"  value="' . $solicitud->getNumeroAbonado() . '" readonly="readonly"/></td>';
                    echo '<td><input type="number" name="numeroOrden" id="numeroOrden" min="1" value="' . $solicitud->getNumeroOrden() . '" /></td>';
                    echo '<td><input type="date" name="fechaSolicitud" id="fechaSolicitud" value="' . $solicitud->getFechaSolicitud() . '"/></td>';
                    echo '<td><input  style="background: #B22222;" type="submit" value="Eliminar" name="eliminar" id="eliminar"/>
                          <input type="submit" style="background: #75B1F2;" value="Editar" name="editar" id="editar"/></td>';
                    echo '</tr>';
                    echo '</form>'; 
                }
            }
        ?>
    </table>
    <?php
                    if (isset($_GET['error'])) {
                        if ($_GET['error'] == "campos") {
                            echo '<p style="color: red">Campo(s) vacio(s)</p>';
                        }else if ($_GET['error'] == "dbError") {
                            echo '<center><p style="color: red">Error en la consulta</p></center>';
                        }else if($_GET['error'] == "formatoPos"){
                            echo '<center><p style="color: red">Numero Abonado y/o Numero Orden deben ser numeros positivos</p></center>';
                        }else if($_GET['error'] == "iguales"){
                            echo '<center><p style="color: red">Numero Orden invalido</p></center>';
                        }
                    } else if (isset($_GET['exito'])) {
                        if ($_GET['exito'] == "inserto") {
                            echo '<p style="color: green">Registrado!</p>';
                        }elseif ($_GET['exito'] == "elimino") {
                            echo '<p style="color: green">Se ha eliminado Correctmente</p>';
                        }elseif ($_GET['exito'] == "edito") {
                            echo '<p style="color: green">Cambios guardados</p>';
                        }
                    }
                    ?>
</body>
</html>